Public Class clsSettings
Private Shared _connectionString as Text = New Text("connectionString","asd",0,True, "Data Source=DILBERT\SQL2012DEV;Initial Catalog=AWSL_Projects;Persist Security Info=True;User ID=sa;Password=23640%i",200)
Public Shared Property connectionString() as Text
	Get
		Return _connectionString
	End Get
	Set (value As Text)
		_connectionString = value
	End Set
End Property
Private Shared _lowestPriorityForLive as Numeric = New Numeric("lowestPriorityForLive","asd",0,True,3,0,10,0)
Public Shared Property lowestPriorityForLive() as Numeric
	Get
		Return _lowestPriorityForLive
	End Get
	Set (value As Numeric)
		_lowestPriorityForLive = value
	End Set
End Property
Private Shared _defaultView as Numeric = New Numeric("defaultView","asd",0,True,0,0,4,0)
Public Shared Property defaultView() as Numeric
	Get
		Return _defaultView
	End Get
	Set (value As Numeric)
		_defaultView = value
	End Set
End Property
Private Shared _checkTest as Binary = New Binary("checkTest","Is this true?",0,True,True)
Public Shared Property checkTest() as Binary
	Get
		Return _checkTest
	End Get
	Set (value As Binary)
		_checkTest = value
	End Set
End Property
Private Shared _newTextSetting as Text = New Text("newTextSetting","New Text Setting",0,True, "Default Value",100)
Public Shared Property newTextSetting() as Text
	Get
		Return _newTextSetting
	End Get
	Set (value As Text)
		_newTextSetting = value
	End Set
End Property
Private Shared _siteName as Text = New Text("siteName","asd",0,True, "Hello World",200)
Public Shared Property siteName() as Text
	Get
		Return _siteName
	End Get
	Set (value As Text)
		_siteName = value
	End Set
End Property
Public Enum opts_defaultMvmt
none
out
inside
End Enum
Private Shared _none as PicklistOption = New PicklistOption("none","None")
Public Shared Property none() as PicklistOption
	Get
		Return _none
	End Get
	Set (value As PicklistOption)
		_none = value
	End Set
End Property
Private Shared _out as PicklistOption = New PicklistOption("out","Outside Movement")
Public Shared Property out() as PicklistOption
	Get
		Return _out
	End Get
	Set (value As PicklistOption)
		_out = value
	End Set
End Property
Private Shared _inside as PicklistOption = New PicklistOption("inside","Inside Movement")
Public Shared Property inside() as PicklistOption
	Get
		Return _inside
	End Get
	Set (value As PicklistOption)
		_inside = value
	End Set
End Property
Private Shared _defaultMvmt as Picklist = New Picklist("defaultMvmt","asd",0,True,2,{none, out, inside})
Public Shared Property defaultMvmt() as Picklist
	Get
		Return _defaultMvmt
	End Get
	Set (value As Picklist)
		_defaultMvmt = value
	End Set
End Property
Private Shared _general as Group = New Group("general","General",1,{connectionString, lowestPriorityForLive, defaultView, checkTest, newTextSetting})
Public Shared Property general() as Group
	Get
		Return _general
	End Get
	Set (value As Group)
		_general = value
	End Set
End Property
Private Shared _site as Group = New Group("site","Site",1,{siteName, defaultMvmt})
Public Shared Property site() as Group
	Get
		Return _site
	End Get
	Set (value As Group)
		_site = value
	End Set
End Property
Private Shared _groupsList as Group() = {general, site}
Public Shared Property groupsList() as Group()
	Get
		Return _groupsList
	End Get
	Set (value As Group())
		_groupsList = value
	End Set
End Property
Public Shared Function getGroupByName(groupName As String)
	For Each group In groupsList
		If group.name = groupName Then
			Return group
		End If
	Next
	Return vbNull
End Function
End Class
