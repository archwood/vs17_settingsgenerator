﻿Imports System.Drawing
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports System.Xml
Imports System.Xml.Serialization

Public Class frm_editor

    Const MAX_TEXTBOX_LENGTH = 50 ' Number of characters

    Const XML_FILE_PATH = "settings.xml"

    Const LABEL_CTRL_SPACING = 5

    Private groupsList As List(Of Group)

    Public Sub New(groups As Group())

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        groupsList = New List(Of Group)

        groupsList.AddRange(groups)
    End Sub

    Private Function getGroupFromTabPage(tbPage As TabPage) As Group

        Dim groupMatch As Match = Regex.Match(tbPage.Name, "(?<=tb_).+")

        Dim groupSearch = groupsList.Find(Function(g) g.name = groupMatch.Value)

        Dim group As Group

        If groupSearch.GetType().Name <> "Group" Then
            Throw New Exception("Group not found in clsSettings for TabPage: " & tbPage.Name)
        End If

        group = groupSearch

        Return group

    End Function


    Private Function areAllSettingsSaved() As Boolean

        For Each tbPage As TabPage In tbctrl.TabPages

            Dim group As Group = getGroupFromTabPage(tbPage)

            For Each ctrl As Control In tbPage.Controls

                Dim settingMatch As Match = Regex.Match(ctrl.Name, "(?<=ctrl_).+") ' Check if ctrl name starts with "ctrl_"

                If Not settingMatch.Success Then ' If not found a ctrl_ control then skip to next
                    Continue For
                End If

                Dim setting = group.getSettingByName(settingMatch.Value)

                Dim settingType = setting.GetType().Name

                If setting Is Nothing Then
                    Throw New Exception("Setting not found in group with name: " & ctrl.Name)
                End If

                Dim controlValue = getControlValue(ctrl, settingType)

                If settingType = "ComPortList" Then ' Annoying thing we have to do because of how we extract the port num from the combobox options

                    Dim comportList As ComPortList = setting
                    controlValue = comportList.getComPortFromControlValue(controlValue)

                ElseIf settingType = "Picklist" Then

                    Dim picklist As Picklist = setting
                    controlValue = picklist.options(controlValue).value ' Get actual value of option

                End If

                If controlValue <> setting.value Then
                    Return False
                End If

            Next

        Next

        Return True

    End Function


    ' Takes the existing clsSettings and edits the existing XML to ensure all settings match up: will be used when software is updated
    ' Will need to call on startup of program as settings will be accessed before editor
    Public Sub updateConfig()

        Dim m_xmld As XmlDocument

        Try

            m_xmld = Setting.getXMLDoc() ' If this throws an error

            For Each group As Group In groupsList ' Cycle through the new configuration as defined by clsSettings

                Dim xGroup As XmlNode = m_xmld.SelectSingleNode("/SettingsFile/Group[@name='" & group.name & "']") ' Get the matching group in the old XML config

                If xGroup Is Nothing Then ' If group is new then we skip
                    Continue For
                End If

                Dim settings As List(Of Setting) = group.getSettings() ' Get all the child settings of the new configuration

                For Each setting As Object In settings

                    ' Look for node in existing XML
                    Dim xSetting As XmlNode = xGroup.SelectSingleNode("children//*[@name='" & setting.name & "']")

                    If xSetting IsNot Nothing Then ' If it exists within the Group's children then we take the existing value and update the clsSettings (using defaultValue as a bypass)
                        setting.defaultValue = xSetting.SelectSingleNode("value").InnerText
                    End If

                Next

            Next

        Catch ex As FileLoadException

            MsgBox("Unable to load config file, generating new config")

        Catch ex As Exception ' TODO - edge test this error handling as I don't really like it

            Throw ex

        Finally

            Dim serializer As XmlSerializer = New XmlSerializer(GetType(Group()), New XmlRootAttribute("SettingsFile"))

            Dim writer As New StreamWriter(XML_FILE_PATH)

            serializer.Serialize(writer, groupsList.ToArray) ' Regardless of what happens we want to serialize the groupsList

            writer.Close() ' Need to close the stream otherwise not accessible

        End Try

    End Sub

    Public Sub addSettingToParent(setting As Setting, parent As Control, ByRef cursor As Point)

        Dim ctrlPosition As Point

        '' LABEL CREATION AND POSITIONING ''

        ' Only create label if not a checkbox as this comes with it's own label property
        If setting.GetType().Name <> "Binary" Then

            Dim label As New Label

            label.AutoSize = True

            label.Location = New Point(cursor.X, cursor.Y)

            label.Text = setting.label

            parent.Controls.Add(label) 'Access tabpage controls and adds button

            ctrlPosition = New Point(10, cursor.Y + label.Height + LABEL_CTRL_SPACING)

            cursor.Y += 35 + label.Height

        Else ' Binary spacing (TODO - Need to sort out properly)

            ctrlPosition = New Point(cursor.X, cursor.Y + LABEL_CTRL_SPACING)

            cursor.Y += 35

        End If

        Dim control ' Defined outside

        Try

            control = setting.getAsControl() ' Gets the main control that defines the setting - attached to setting classes to remove need to edit in multiple places

            control.Location = ctrlPosition 'Sets position of the control

            control.Name = "ctrl_" & setting.name ' All setting controls will follow format of ctrl and then given camelCase setting name

            parent.Controls.Add(control)


            If setting.GetType().Name = "PathLocation" Then ' Need to add button additionally for PathLocation - defined here as we need to AddHandler

                Dim dialogButton As New Button

                dialogButton.Name = "btn_" & setting.name

                dialogButton.Text = "Browse"

                dialogButton.Location = New Point(ctrlPosition.X + control.width + 10, ctrlPosition.Y) ' Same line as control just further right

                AddHandler dialogButton.Click, Sub(sender, e) btn_dialog_click(sender, e, setting, control) ' Adds event to button to open relevant dialog to choose path value

                parent.Controls.Add(dialogButton) ' Add button to form

            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub openEditor(userAccessLevel As Int32)

        Dim sbSetting As New System.Text.StringBuilder ' This is for composing more accurate error msgs

        ' TODO - add check here that the XML is in the correct place?

        ' TODO - add feedback to use that form is loading

        ' TODO - improve form load time - maybe just load form placements on the startup and then openEditor will be used to refresh the control values using ctrl_SETTINGNAME and getsettingbyname
        Try

            For Each group In groupsList

                If group.accessLevel > userAccessLevel Then  'prevents access to those not authorised
                    Continue For
                End If

                ' Create TabPage to hold all group children

                Dim page As TabPage = New TabPage(group.label)

                page.Name = "tb_" & group.name

                page.AutoScroll = True ' Allows the page to be scrollable if controls are placed outside the visible boundaries of the page 

                tbctrl.TabPages.Add(page) ' Add TabPage to TabControl


                ' Initialise control cursors for positioning labels and controls
                Dim labelX As Int32 = 10
                Dim labelY As Int32 = 20
                Dim cursor As New Point(labelX, labelY)


                For Each child In group.children  'Adds each setting from the group with the appropriate control and label

                    If child.GetType().Name = "Subgroup" Then ' Create groupbox and populate with settings

                        Dim subgroup As Subgroup = child

                        Dim subgroupBox As New GroupBox

                        subgroupBox.Text = subgroup.label

                        subgroupBox.AutoSize = True

                        subgroupBox.Location = cursor

                        cursor.Y += LABEL_CTRL_SPACING

                        cursor.X += 10 ' Indent cursor for within subgroup

                        Dim subSettings = subgroup.getSettings()

                        Dim subGroupCursor = New Point(10, 20) ' Needs new cursor within the subgroup

                        For Each subSetting As Setting In subSettings ' For now, don't care about subgroups within subgroups

                            If userAccessLevel < subSetting.accessLevel And Not subSetting.viewIfNoAccess Then ' check if the user has the correct permissions to view the setting
                                Continue For
                            End If

                            ' For accurate error messages
                            sbSetting.Clear()
                            sbSetting.Append(group.name & " / " & subSetting.name)

                            addSettingToParent(subSetting, subgroupBox, subGroupCursor) ' Configure relevant label and controls for the subsetting

                        Next

                        cursor.X -= 10 ' Unindent cursor

                        cursor.Y = subgroupBox.Location.Y + subgroupBox.PreferredSize.Height + LABEL_CTRL_SPACING

                        page.Controls.Add(subgroupBox) ' Add SubGroup to Group tab

                    Else

                        Dim setting As Setting = child ' convert child to setting

                        ' check if the user has the correct permissions to view the setting
                        If userAccessLevel < setting.accessLevel And Not setting.viewIfNoAccess Then
                            Continue For
                        End If

                        ' For accurate error messages
                        sbSetting.Clear()
                        sbSetting.Append(group.name & " / " & child.name)

                        addSettingToParent(setting, page, cursor) ' Configure relevant label and controls for the subsetting

                    End If

                Next

            Next

        Catch ex As Exception
            clsAppFunctions.ErrMsg(Me, "openEditor()" & sbSetting.ToString, ex)
            Return
        End Try

        Me.Show() ' If successful then show the generated form

    End Sub

    Private Sub setSettingValue(group As Group, ctrl As Control)

        Dim settingMatch As Match = Regex.Match(ctrl.Name, "(?<=ctrl_).+") ' Check if ctrl name starts with "ctrl_"

        If Not settingMatch.Success Then ' If not found a ctrl_ control then skip to next
            Return
        End If

        Dim setting = group.getSettingByName(settingMatch.Value)

        If setting Is Nothing Then
            Throw New Exception("Setting not found in group with name: " & ctrl.Name)
        End If

        Dim settingType = setting.GetType().Name

        Dim controlValue = getControlValue(ctrl, settingType) ' This gets the value depending on the control type

        ' NOTE - this now doesn't work if we're using the same control type for different setting types i.e. combo box for both picklist and ComPortList (and will need adjustments for PrinterList as well)

        If settingType = "ComPortList" Then ' Get COM port number from selectedItem - might be a better way of doing this

            Dim port As Int32 = setting.getComPortFromControlValue(controlValue)

            controlValue = port

        ElseIf settingType = "Picklist" Then

            Dim selectedOption As PicklistOption = setting.options(controlValue)

            controlValue = selectedOption.value ' Amends savedvalue to actual value of option rather than its index within .options

        End If

        setting.value = controlValue ' Set value of setting to control value, which should then write to XML

    End Sub

    ' For each tabpage we go through all the controls
    ' only look for controls with name starting "ctrl_"
    ' once a ctrl has been found, we pull the corresponding setting from clsSettings as the names will match
    ' Each tabpage has the format "tb_[GROUP_NAME]"
    ' We can use clsSettings.getGroupByName(groupName) and then Group.getSettingByName(settingName) to pull the exact object that matches with the control
    ' Now we can convert the current value of the control into the appropriate type to set the value in the setting and the class will handle the actual saving to XML
    ' Will need some sort of validate function to ensure that the value adheres to all the setting constraints and matches the type
    Private Sub saveSettings() ' Will replace readInputs() below 

        ' Cycle through all tabpages

        For Each tbPage As TabPage In tbctrl.TabPages

            Dim group As Group = getGroupFromTabPage(tbPage)

            For Each ctrl As Control In tbPage.Controls

                If ctrl.GetType().Name = "GroupBox" Then

                    Dim gb As GroupBox = ctrl

                    For Each subCtrl As Control In gb.Controls
                        setSettingValue(group, subCtrl)
                    Next

                Else

                    setSettingValue(group, ctrl)

                End If

            Next

        Next

    End Sub

    Private Function getControlValue(control As Control, settingType As String)

        Dim type = control.GetType()

        Select Case type.Name
            Case "TextBox" ' This one is a bit pointless btu might as well keep consistent
                Dim convertedControl As TextBox = control
                Return convertedControl.Text
            Case "NumericUpDown"
                Dim convertedControl As NumericUpDown = control
                Return convertedControl.Value
            Case "CheckBox"
                Dim convertedControl As CheckBox = control
                Return convertedControl.Checked
            Case "ComboBox"

                Dim convertedControl As ComboBox = control

                If settingType = "Picklist" Then
                    Return convertedControl.SelectedIndex
                ElseIf settingType = "ComPortList" Or settingType = "PrinterList" Then
                    Return convertedControl.SelectedItem
                End If

            Case Else
                Return control.Text
        End Select

    End Function

    ''' <summary>
    ''' Allows us to open a file or folder dialog to update the value of a PathLocation setting type
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <param name="setting">PathLocation setting object</param>
    ''' <param name="control">TextBox control that path string is stored in before saving settings</param>
    Private Sub btn_dialog_click(sender As Object, e As EventArgs, setting As PathLocation, control As TextBox)
        ' Event handler code here, with access to myParameter
        If setting.isFilePath Then
            of_dialog.FileName = setting.value
            If of_dialog.ShowDialog() = DialogResult.OK Then
                control.Text = of_dialog.FileName
            End If
        Else
            fb_dialog.RootFolder = Environment.SpecialFolder.MyComputer
            fb_dialog.SelectedPath = setting.value
            If fb_dialog.ShowDialog() = DialogResult.OK Then
                control.Text = fb_dialog.SelectedPath
            End If
        End If
    End Sub

    'Confirm Changes button - Makes sure user wants to confirm change and then makes approriate checks before writing to the file and generating the classes
    Private Sub btn_confirm_Click(sender As Object, e As EventArgs) Handles btn_confirm.Click

        Dim confirm As MsgBoxResult = MsgBox("Are you sure you want to confirm changes?", MsgBoxStyle.YesNo, "Confirm Changes")

        If confirm = MsgBoxResult.Yes Then
            saveSettings()
            MsgBox("All changes have been saved. Changes will take effect after restarting the program.")
            Me.Close()
        End If

    End Sub

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        ' User is prompted to save changes or discard changes 
        If Not areAllSettingsSaved() Then
            Dim result = MsgBox("Not all settings are saved, do you wish to discard changes?", MsgBoxStyle.YesNo, "Discard changes?")
            If result = MsgBoxResult.No Then
                Return
            End If
        End If

        Me.Close()

    End Sub

End Class
