﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_editor))
        Me.tbctrl = New System.Windows.Forms.TabControl()
        Me.btn_confirm = New System.Windows.Forms.Button()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.fb_dialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.of_dialog = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'tbctrl
        '
        Me.tbctrl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbctrl.Location = New System.Drawing.Point(12, 12)
        Me.tbctrl.Name = "tbctrl"
        Me.tbctrl.SelectedIndex = 0
        Me.tbctrl.Size = New System.Drawing.Size(483, 237)
        Me.tbctrl.TabIndex = 0
        '
        'btn_confirm
        '
        Me.btn_confirm.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_confirm.Location = New System.Drawing.Point(318, 262)
        Me.btn_confirm.Name = "btn_confirm"
        Me.btn_confirm.Size = New System.Drawing.Size(96, 23)
        Me.btn_confirm.TabIndex = 1
        Me.btn_confirm.Text = "Save Changes"
        Me.btn_confirm.UseVisualStyleBackColor = True
        '
        'btn_cancel
        '
        Me.btn_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancel.Location = New System.Drawing.Point(420, 262)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancel.TabIndex = 2
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'of_dialog
        '
        Me.of_dialog.FileName = "OpenFileDialog1"
        '
        'mainEditor
        '
        Me.AcceptButton = Me.btn_confirm
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btn_cancel
        Me.ClientSize = New System.Drawing.Size(510, 292)
        Me.Controls.Add(Me.btn_cancel)
        Me.Controls.Add(Me.btn_confirm)
        Me.Controls.Add(Me.tbctrl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainEditor"
        Me.Text = "Settings"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents tbctrl As TabControl
    Friend WithEvents btn_confirm As Button
    Friend WithEvents btn_cancel As Button
    Friend WithEvents fb_dialog As FolderBrowserDialog
    Friend WithEvents of_dialog As OpenFileDialog
End Class
