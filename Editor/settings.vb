﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Drawing

<Serializable()>
Public MustInherit Class Child
    Private _name As String
    Private _label As String
    Private _accessLevel As Integer

    <XmlAttribute>
    Public Property name() As String

        Get
            Return _name
        End Get

        Set(value As String)

            If String.IsNullOrWhiteSpace(value) Then
                Throw New Exception("Child name cannot be empty")
            End If

            ' Setting name cannot have whitespace inside
            If value.Split(" ").Length > 1 Then
                Throw New Exception("Child name cannot include whitespace")
            End If

            ' Restrict to only alphabetical characters
            If Not Regex.IsMatch(value, "^[a-zA-Z][a-zA-Z0-9]*$") Then
                Throw New Exception("Child name cannot include non-alphanumeric characters or start with a number")
            End If

            _name = value
        End Set
    End Property

    <XmlAttribute>
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(value As String)
            _label = value
        End Set
    End Property

    <XmlAttribute>
    Public Property accessLevel() As Integer
        Get
            Return _accessLevel
        End Get
        Set(value As Int32)
            _accessLevel = value
        End Set
    End Property

    Public MustOverride Function getConstructorString() As String


End Class

#Region "Basic Settings"
<Serializable()>
Public MustInherit Class Setting
    Inherits Child

    Public Const XML_FILE_NAME As String = "settings.xml"

    Private _viewIfNoAccess As Boolean

    <XmlElement>
    Public Property viewIfNoAccess() As Boolean
        Get
            Return _viewIfNoAccess
        End Get
        Set(value As Boolean)
            _viewIfNoAccess = value
        End Set
    End Property

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean)
        MyBase.name = name
        MyBase.label = label
        MyBase.accessLevel = accessLevel
        _viewIfNoAccess = viewIfNoAccess

    End Sub

    Public Sub New()
        'Used for xml serializer
    End Sub

    Public MustOverride Function getAsControl() As Control




    Public Shared Function getXMLDoc() As XmlDocument

        Dim m_xmld As XmlDocument = New XmlDocument()

        Try
            m_xmld.Load(XML_FILE_NAME)
        Catch ex As Exception
            Throw New FileLoadException("Error loading XML file (""" & XML_FILE_NAME & """) readXMLValue(): " & ex.Message)
        End Try

        Return m_xmld

    End Function

    Private Function getXMLNode(m_xmld As XmlDocument) As XmlNode

        Dim m_node As XmlNode = m_xmld.SelectSingleNode("/SettingsFile/Group/children//*[@name='" & Me.name & "']")

        If m_node Is Nothing Then
            Throw New Exception("Node for " & Me.name & "Not found in XML file")
        End If

        Return m_node

    End Function


    Public Function readXMLProperty(propertyName As String) As String

        Dim m_xmld As XmlDocument = getXMLDoc()

        Dim m_node As XmlNode = getXMLNode(m_xmld)

        Return m_node.SelectSingleNode(propertyName).InnerText

    End Function

    Public Sub writeXMLProperty(propertyName As String, newValue As String)

        Dim m_xmld As XmlDocument = getXMLDoc()

        Dim m_node As XmlNode = getXMLNode(m_xmld)

        m_node.SelectSingleNode(propertyName).InnerText = newValue

        m_xmld.Save(XML_FILE_NAME)

    End Sub

End Class

<Serializable>
Public Class Text
    Inherits Setting

    Private _value As String

    <XmlIgnore>
    Public Property value() As String
        Get
            Dim xmlValue As String = readXMLProperty("value")

            Dim converted = xmlValue

            Return converted

        End Get

        Set(setter As String)

            ' Ensure that the provided string adheres to the settings properties
            If setter.Length <= maxLength Then
                writeXMLProperty("value", setter)
            Else
                Throw New Exception("String provided exceeds setting's maximum length")
            End If

        End Set

    End Property

    <XmlElement>
    Private _maxLength As Int32
    Public Property maxLength() As Int32
        Get
            Return _maxLength
        End Get
        Set(value As Int32)
            _maxLength = value
        End Set
    End Property

    <XmlElement("value")>
    Public Property defaultValue() As String ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As String)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New Text (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & ", """ & defaultValue.ToString & """," & maxLength.ToString & ")"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Const MAX_TEXTBOX_LENGTH = 50 ' Number of characters

        Dim control = New TextBox

        control.MaxLength = maxLength 'Validation

        control.Text = value

        ' Resizes width up to maximum character length permitted by setting and program
        If maxLength > MAX_TEXTBOX_LENGTH Then
            control.Width = MAX_TEXTBOX_LENGTH * control.Font.Size
        ElseIf maxLength < 0 Then
            control.Width = 255
        Else
            control.Width = maxLength * control.Font.Size
        End If

        Return control


    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As String, maxLength As Int32)
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _maxLength = maxLength
        _value = value
    End Sub

    Private Sub New()
        MyBase.New()
    End Sub

End Class

<Serializable>
Public Class Numeric
    Inherits Setting

    Private _value As Decimal

    <XmlIgnore>
    Public Property value() As Decimal

        Get

            Dim xmlValue As String = readXMLProperty("value")

            Dim converted = Convert.ToDecimal(xmlValue)

            Return converted

        End Get

        Set(setter As Decimal)

            If setter > maxValue And setter < minValue Then
                Throw New ArgumentOutOfRangeException("Decimal value exceeds defined boundaries")
            End If

            '' TODO enforce decimal place check

            writeXMLProperty("value", setter.ToString())
        End Set

    End Property

    ' Int32 representing the number of decimal places the value is stored as
    <XmlElement>
    Private _decimalPlaces As Int32
    Public Property decimalPlaces() As Int32
        Get
            Return _decimalPlaces
        End Get
        Set(value As Int32)
            If value < 0 Then
                Throw New ArgumentOutOfRangeException("Number of decimal places cannot be less than zero")
            End If

            _decimalPlaces = value
        End Set
    End Property

    ' Self explanatory

    Private _maxValue As Decimal

    <XmlElement>
    Public Property maxValue() As Decimal
        Get
            Return _maxValue
        End Get
        Set(value As Decimal)
            _maxValue = value
        End Set
    End Property

    ' Self explanatory

    Private _minValue As Decimal

    <XmlElement>
    Public Property minValue() As Decimal
        Get
            Return _minValue
        End Get
        Set(value As Decimal)
            _minValue = value
        End Set
    End Property

    <XmlElement("value")>
    Public Property defaultValue() As Decimal ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As Decimal)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New Numeric (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & "," & defaultValue.ToString & "," & decimalPlaces.ToString & "," & maxValue.ToString & "," & minValue.ToString & ")"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New NumericUpDown
        control.Maximum = maxValue
        control.Minimum = minValue
        control.Value = value
        control.DecimalPlaces = decimalPlaces
        control.Increment = 0.1 ^ decimalPlaces ' Might need to add this to the Numeric structure (int/decimal)

        Return control

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As Decimal, decimalPlaces As Int32, maxValue As Decimal, minValue As Decimal)

        MyBase.New(name, label, accessLevel, viewIfNoAccess)

        If minValue > maxValue Then
            Throw New ArgumentException("MinValue cannot be greater than MaxValue")
        End If

        _value = value
        _decimalPlaces = decimalPlaces
        _maxValue = maxValue
        _minValue = minValue
    End Sub

    Private Sub New()

    End Sub

    '' valueAsInt32? Function to return value as Int32 rather than Decimal, see if there is a use case when converting the software

End Class

<Serializable>
Public Class Binary
    Inherits Setting

    Private _value As Boolean

    <XmlIgnore>
    Public Property value() As Boolean
        Get
            Dim xmlValue As String = readXMLProperty("value")
            Dim converted
            converted = Convert.ToBoolean(xmlValue)
            Return converted
        End Get
        Set(setter As Boolean)
            writeXMLProperty("value", setter.ToString())
        End Set
    End Property

    <XmlElement("value")>
    Public Property defaultValue() As Boolean ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As Boolean)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New Binary (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & "," & defaultValue.ToString & ")"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New CheckBox  'Changes to correct control type
        control.Text = label
        control.Checked = value
        control.AutoSize = True
        control.CheckAlign = ContentAlignment.MiddleRight

        Return control

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, value As Boolean, viewIfNoAccess As Boolean)
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _value = value
    End Sub

    Private Sub New()

    End Sub

End Class

<Serializable>
Public Class Picklist
    Inherits Setting

    Private _value As Int32
    <XmlIgnore>
    Public Property value() As Int32
        Get
            Dim xmlValue As String = readXMLProperty("value")
            Dim converted
            converted = Convert.ToInt32(xmlValue)
            Return converted
        End Get
        Set(setter As Int32)
            writeXMLProperty("value", setter.ToString())
        End Set
    End Property

    Private _options As List(Of PicklistOption)

    <XmlArray>
    Public Property options() As List(Of PicklistOption)
        Get
            Return _options
        End Get
        Set(value As List(Of PicklistOption))
            _options = value
        End Set
    End Property

    Public Function getOptionLabels() As String() ' Returns a String() list of the option labels to directly supply editor CB items collection

        Dim optionLabels = New List(Of String)

        For Each opt In Me.options
            optionLabels.Add(opt.label)
        Next

        Return optionLabels.ToArray

    End Function

    Public Function getOptionByValue(val As Int32) As PicklistOption
        Return Me.options.Find(Function(o As PicklistOption)
                                   Return o.value = val
                               End Function)
    End Function

    <XmlElement("value")>
    Public Property defaultValue() As Int32 ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As Int32)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim optionNames = options.Select(Of String)(Function(o) Me.name & "_" & o.name)

        Dim optionsString As String = Join(optionNames.ToArray, ", ") ' List of the option variable names

        Dim constructorString As String = "New Picklist (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & "," & defaultValue.ToString & ",{" & optionsString & "})"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New ComboBox

        Dim labels = getOptionLabels()
        control.Items.AddRange(labels) ' Get list of option labels and use as collection of items
        Dim selectedOption As PicklistOption = getOptionByValue(value)
        control.SelectedItem = selectedOption.label
        control.DropDownStyle = ComboBoxStyle.DropDownList ' This prevents the user from free-form input

        Return control

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As Int32, options As PicklistOption())
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _options = New List(Of PicklistOption)
        _options.AddRange(options)
        _value = value ' Selected index in options
    End Sub

    Public Sub New()
        ' Required for XMLSerializer
    End Sub

End Class

<Serializable>
<XmlType("option")>
Public Class PicklistOption

    Private _name As String
    <XmlAttribute>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value ' Passed validation
        End Set
    End Property

    Private _label As String
    <XmlText>
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(value As String)
            _label = value
        End Set
    End Property


    Private _value As Int32
    <XmlAttribute>
    Public Property value() As Integer
        Get
            Return _value
        End Get
        Set(value As Integer)
            _value = value
        End Set
    End Property

    Public Function getConstructorString() As String

        Dim constructorString As String = "New PicklistOption(""" & name & """,""" & label & """, " & value.ToString & ")"

        Return constructorString

    End Function

    Public Sub New(name As String, label As String, value As Int32)
        _name = name
        _label = label
        _value = value

    End Sub
    Private Sub New()

    End Sub

End Class

#End Region

#Region "Extended Settings"
<Serializable>
Public Class ComPortList
    Inherits Setting

    Private _value As Int32
    <XmlIgnore>
    Public Property value() As Int32
        Get
            Dim xmlValue As String = readXMLProperty("value")
            Dim converted
            converted = Convert.ToInt32(xmlValue)
            Return converted
        End Get
        Set(setter As Int32)
            writeXMLProperty("value", setter.ToString())
        End Set
    End Property

    Public Function getComPortNames() As String()
        ' Do I have this as a Key-Value Pair? With the port number actually pulled out
        Dim portNames = My.Computer.Ports.SerialPortNames
        Dim nameList As New List(Of String)
        nameList.AddRange(portNames)
        Return nameList.ToArray()
    End Function

    Public Function getComPortFromControlValue(controlValue As String) As Int32
        Dim portMatch As Match = Regex.Match(controlValue, "(?<=COM)[0-9]+") ' Look for the number after COM in the list of ports

        If Not portMatch.Success Then
            Throw New Exception("Port number not found on " & Me.name) ' All items in the list should have a number after them so this is a fatal error if not
        End If

        Return Convert.ToInt32(portMatch.Value)

    End Function

    <XmlElement("value")>
    Public Property defaultValue() As Int32 ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As Int32)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New ComPortList (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & "," & defaultValue.ToString & ")"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New ComboBox

        Dim labels As String() = getComPortNames()

        control.Items.AddRange(labels) ' Get list of option labels and use as collection of items

        Dim currentPortName As String = "COM" & value.ToString()

        If Not labels.Contains(currentPortName) Then ' If the pulled list of port names does not contain the set value then add option but notify that this port is not on the machine
            control.Items.Add(currentPortName & ": NOT FOUND")
            control.SelectedItem = currentPortName & ": NOT FOUND"
        Else
            control.SelectedItem = currentPortName
        End If

        control.Width = 180

        control.DropDownStyle = ComboBoxStyle.DropDownList ' This prevents the user from free-form input

        Return control

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As Int32)
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _value = value ' Default Com Port
    End Sub
    Private Sub New()

    End Sub
End Class

<Serializable>
Public Class PrinterList
    Inherits Setting

    Private _value As String
    <XmlIgnore>
    Public Property value() As String
        Get
            Dim xmlValue As String = readXMLProperty("value")
            Dim converted = xmlValue
            Return converted
        End Get
        Set(setter As String)
            writeXMLProperty("value", setter.ToString())
        End Set
    End Property

    Public Shared Function getPrinterNames() As String()
        Dim printerNames = Printing.PrinterSettings.InstalledPrinters
        Dim nameArr = printerNames.Cast(Of String)().ToArray()
        Return nameArr
    End Function

    <XmlElement("value")>
    Public Property defaultValue() As String ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As String)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New PrinterList (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & ",""" & defaultValue.ToString & """)"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New ComboBox

        control.Width = 200

        control.DropDownStyle = ComboBoxStyle.DropDownList ' This prevents the user from free-form input

        Dim labels As String() = getPrinterNames()

        control.Items.Add("") ' Add blank option at 0 index

        control.Items.AddRange(labels) ' Get list of option labels and use as collection of items

        If String.IsNullOrEmpty(value) Then
            control.SelectedIndex = 0
        Else
            control.SelectedItem = value
        End If

        Return control

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As String)
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _value = value ' Default Printer
    End Sub

    Private Sub New()

    End Sub
End Class

<Serializable>
Public Class PathLocation
    Inherits Setting

    Private _value As String

    <XmlIgnore>
    Public Property value() As String
        Get
            Dim xmlValue As String = readXMLProperty("value")
            Dim converted = xmlValue
            Return converted
        End Get
        Set(setter As String)
            writeXMLProperty("value", setter.ToString())
        End Set
    End Property

    Private _isFilePath As Boolean
    <XmlElement>
    Public Property isFilePath As Boolean ' Indicates whether we should use a open file dialog or if false use an open folder dialog
        Get
            Return _isFilePath
        End Get
        Set(value As Boolean)
            _isFilePath = value
        End Set
    End Property

    <XmlElement("value")>
    Public Property defaultValue() As String ' This is for use primarily in the Config utility
        Get
            Return _value
        End Get
        Set(defaultValue As String)
            _value = defaultValue
        End Set
    End Property

    Public Overrides Function getConstructorString() As String

        Dim constructorString As String = "New PathLocation (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & "," & viewIfNoAccess.ToString & ",""" & defaultValue.ToString & """, " & isFilePath.ToString & ")"

        Return constructorString

    End Function

    Public Overrides Function getAsControl() As Control

        Dim control = New TextBox

        control.Width = 300

        control.Text = value

        Return control

    End Function


    Public Sub New(name As String, label As String, accessLevel As Int32, viewIfNoAccess As Boolean, value As String, isFilePath As Boolean)
        MyBase.New(name, label, accessLevel, viewIfNoAccess)
        _value = value ' Default path
        _isFilePath = isFilePath
    End Sub

    Private Sub New()

    End Sub
End Class


#End Region

<Serializable>
Public Class Group

    Private _name As String
    <XmlAttribute>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(value As String)

            If String.IsNullOrWhiteSpace(value) Then
                Throw New Exception("Group name cannot be empty")
            End If

            ' Setting name cannot have whitespace inside
            If value.Split(" ").Length > 1 Then
                Throw New Exception("Group name cannot include whitespace")
            End If

            ' Restrict to only alphabetical characters
            If Not Regex.IsMatch(value, "^[a-zA-Z][a-zA-Z0-9]*$") Then
                Throw New Exception("Group name cannot include non-alphabetical characters or start with a number")
            End If

            _name = value

        End Set
    End Property

    Private _label As String
    <XmlAttribute>
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(value As String)
            _label = value
        End Set
    End Property

    Private _accessLevel As Int32
    <XmlAttribute>
    Public Property accessLevel() As Int32
        Get
            Return _accessLevel
        End Get
        Set(value As Int32)
            _accessLevel = value
        End Set
    End Property

    Private _children As List(Of Child)

    <XmlArray>
    <XmlArrayItem(GetType(Text))>
    <XmlArrayItem(GetType(Numeric))>
    <XmlArrayItem(GetType(Binary))>
    <XmlArrayItem(GetType(Picklist))>
    <XmlArrayItem(GetType(ComPortList))>
    <XmlArrayItem(GetType(PrinterList))>
    <XmlArrayItem(GetType(PathLocation))>
    <XmlArrayItem(GetType(Subgroup))>
    Public Property children() As List(Of Child)
        Get
            Return _children
        End Get
        Set(value As List(Of Child))
            _children = value
        End Set
    End Property

    Public Function getConstructorString() As String

        Dim childrenNames = children.Select(Of String)(Function(x) x.name)

        Dim childrenString As String = Join(childrenNames.ToArray, ", ")

        'Group creation:
        Dim constructorString As String = "New Group (" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & ",{" & childrenString & "})"

        Return constructorString

    End Function
    Public Sub New(name As String, label As String, accessLevel As Int32, children As Child())

        _name = name

        _accessLevel = accessLevel

        _label = label

        _children = New List(Of Child)

        If children.Length > 0 Then
            _children.AddRange(children) ' Convert array to list - reason is list is more useful but array is needed in literal definition
        Else
            Throw New Exception("children empty: Group must have at least one child")
        End If

    End Sub

    Private Sub New()
        ' Required for xml serialization as uses reflection - whatever that means
    End Sub

    Public Function getChildByName(childName As String)

        For Each child In children

            If child.name = childName Then
                Return child
            End If

            If child.GetType().Name = "Subgroup" Then

                Dim subgroup As Subgroup = child

                Dim search = subgroup.getChildByName(childName)

                If subgroup IsNot Nothing Then
                    Return search
                End If

            End If

        Next

        Return Nothing ' Returns null if child not found in group

    End Function

    Public Function getSettingByName(settingName As String) ' Returns setting if found in children (including within Subgroup children) and Nothing if not found

        For Each child In children

            If child.GetType().Name = "Subgroup" Then

                ' Then search through it's children

                Dim subgroup As Subgroup = child

                Dim search = subgroup.getSettingByName(settingName)

                If search IsNot Nothing Then
                    Return search
                End If

            ElseIf child.name = settingName Then

                Return child

            End If

        Next

        Return Nothing ' Returns null if Setting not found in group

    End Function

    Public Function getSettings() As List(Of Setting)

        Dim settings As New List(Of Setting)

        For Each child As Child In children

            If child.GetType().Name = "Subgroup" Then
                Dim subgroup As Subgroup = child
                settings.AddRange(subgroup.getSettings())
            Else
                Dim setting As Setting = child
                settings.Add(setting)
            End If

        Next

        Return settings

    End Function

End Class

<Serializable>
Public Class Subgroup
    Inherits Child

    Private _children As List(Of Child)

    <XmlArray>
    <XmlArrayItem(GetType(Text))>
    <XmlArrayItem(GetType(Numeric))>
    <XmlArrayItem(GetType(Binary))>
    <XmlArrayItem(GetType(Picklist))>
    <XmlArrayItem(GetType(ComPortList))>
    <XmlArrayItem(GetType(PrinterList))>
    <XmlArrayItem(GetType(PathLocation))>
    <XmlArrayItem(GetType(Subgroup))>
    Public Property children() As List(Of Child)
        Get
            Return _children
        End Get
        Set(value As List(Of Child))
            _children = value
        End Set
    End Property

    Public Function getSettingByName(settingName As String)

        For Each child In children

            If child.name = settingName Then
                Return child
            End If

            If child.GetType().Name = "Subgroup" Then

                ' Then search through it's children

                Dim subgroup As Subgroup = child

                Dim search = subgroup.getSettingByName(settingName)

                If search IsNot Nothing Then
                    Return search
                End If

            End If

        Next

        Return Nothing ' Returns null if Setting not found in group

    End Function

    Public Function getChildByName(childName As String)

        For Each child In children

            If child.GetType().Name = "Subgroup" Then

                Dim subgroup As Subgroup = child

                Dim search = subgroup.getChildByName(childName)

                If subgroup IsNot Nothing Then
                    Return search
                End If

            ElseIf child.name = childName Then

                Return child

            End If

        Next

        Return Nothing ' Returns null if child not found in subgroup

    End Function

    Public Function getSettings() As List(Of Setting)

        Dim settings As New List(Of Setting)

        For Each child As Child In children

            If child.GetType().Name = "Subgroup" Then
                Dim subgroup As Subgroup = child
                settings.AddRange(subgroup.getSettings())

            Else
                Dim setting As Setting = child
                settings.Add(setting)
            End If

        Next

        Return settings

    End Function

    Public Overrides Function getConstructorString() As String

        Dim childrenNames = children.Select(Of String)(Function(x) x.name)

        Dim childrenString As String = Join(childrenNames.ToArray, ", ")

        'Group creation:
        Dim constructorString As String = "New Subgroup(" & """" & name & """" & "," & """" & label & """" & "," & accessLevel.ToString & ",{" & childrenString & "})"

        Return constructorString

    End Function

    Public Sub New(name As String, label As String, accessLevel As Int32, children As Child())

        MyBase.name = name

        MyBase.accessLevel = accessLevel

        MyBase.label = label

        _children = New List(Of Child)

        If children.Length > 0 Then
            _children.AddRange(children) ' Convert array to list - reason is list is more useful but array is needed in literal definition
        Else
            Throw New Exception("children empty: Subgroup must have at least one child")
        End If

    End Sub

    Private Sub New()
        ' Required for XMLSerializer
    End Sub

End Class



