Imports System.Xml
Public Class Setting

Private  _name as String
Public  Property name() as String
	Get
		Return _name
	End Get
	Set(value As String)
		_name = value
	End Set
End Property
Private  _accessLevel as String
Public  Property accessLevel() as String
	Get
		Return _accessLevel
	End Get
	Set(value As String)
		_accessLevel = value
	End Set
End Property
Private  _viewIfNoAccess as Boolean
Public  Property viewIfNoAccess() as Boolean
	Get
		Return _viewIfNoAccess
	End Get
	Set(value As Boolean)
		_viewIfNoAccess = value
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,viewIfNoAccess as Boolean)
		_name = name
		_accessLevel = accessLevel
		_viewIfNoAccess = viewIfNoAccess
	End Sub
	Public Function readXMLValue(settingName As String) As String
		Dim m_xmld As XmlDocument = New XmlDocument()
		Dim m_nodelist_settings As XmlNodeList
		Dim m_node As XmlNode
		m_xmld.Load("settingsFile.xml")
		m_nodelist_settings = m_xmld.SelectNodes(" / SettingsFile / Group / Setting")
		Dim output As String = ""
		For Each m_node In m_nodelist_settings
			Dim name As String = m_node.Attributes.GetNamedItem("name").Value
			If name = settingName Then
				output = m_node.ChildNodes(2).InnerText
				Exit For
			End If
		Next
		Return output
	End Function
	Public Sub writeXML(settingName As String, newValue As String)
		Dim m_xmld As XmlDocument = New XmlDocument()
		Dim m_nodelist_settings As XmlNodeList
		Dim m_node As XmlNode
		m_xmld.Load("settingsFile.xml")
		m_nodelist_settings = m_xmld.SelectNodes(" / SettingsFile / Group / Setting")
		For Each m_node In m_nodelist_settings
			Dim name As String = m_node.Attributes.GetNamedItem("name").Value
			If settingName = name Then
				m_node.ChildNodes(2).InnerText = newValue
			End If
		Next
		m_xmld.Save("settingsFile.xml")
	End Sub
End Class

Public Class Text
	Inherits Setting

Private _value as String
Public Property value() as String
	Get
		Dim xmlValue as String =readXMLValue(name)
		Dim converted
		converted= xmlValue
		Return converted
	End Get
	Set(setter as String)
		writeXML(name,setter.ToString())
	End Set
End Property
Private  _maxLength as Int32
Public  Property maxLength() as Int32
	Get
		Return _maxLength
	End Get
	Set(value As Int32)
		_maxLength = value
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,viewIfNoAccess as Boolean,maxLength as Int32)
		MyBase.New(name,accessLevel,viewIfNoAccess)
		_maxLength = maxLength
	End Sub
End Class

Public Class Numeric
	Inherits Setting

Private _value as Decimal
Public Property value() as Decimal
	Get
		Dim xmlValue as String =readXMLValue(name)
		Dim converted
		converted= Convert.ToDecimal(xmlValue)
		Return converted
	End Get
	Set(setter as Decimal)
		writeXML(name,setter.ToString())
	End Set
End Property
Private  _length as Int32
Public  Property length() as Int32
	Get
		Return _length
	End Get
	Set(value As Int32)
		_length = value
	End Set
End Property
Private  _decimalPlaces as Int32
Public  Property decimalPlaces() as Int32
	Get
		Return _decimalPlaces
	End Get
	Set(value As Int32)
		_decimalPlaces = value
	End Set
End Property
Private  _maxValue as Decimal
Public  Property maxValue() as Decimal
	Get
		Return _maxValue
	End Get
	Set(value As Decimal)
		_maxValue = value
	End Set
End Property
Private  _minValue as Decimal
Public  Property minValue() as Decimal
	Get
		Return _minValue
	End Get
	Set(value As Decimal)
		_minValue = value
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,viewIfNoAccess as Boolean,length as Int32,decimalPlaces as Int32,maxValue as Decimal,minValue as Decimal)
		MyBase.New(name,accessLevel,viewIfNoAccess)
		_length = length
		_decimalPlaces = decimalPlaces
		_maxValue = maxValue
		_minValue = minValue
	End Sub
End Class

Public Class Binary
	Inherits Setting

Private _value as Boolean
Public Property value() as Boolean
	Get
		Dim xmlValue as String =readXMLValue(name)
		Dim converted
		converted= Convert.ToBoolean(xmlValue)
		Return converted
	End Get
	Set(setter as Boolean)
		writeXML(name,setter.ToString())
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,viewIfNoAccess as Boolean)
		MyBase.New(name,accessLevel,viewIfNoAccess)
	End Sub
End Class

Public Class Picklist
	Inherits Setting

Private _value as Int32
Public Property value() as Int32
	Get
		Dim xmlValue as String =readXMLValue(name)
		Dim converted
		Return converted
	End Get
	Set(setter as Int32)
		writeXML(name,setter.ToString())
	End Set
End Property
Private  _enumList as PicklistEnum
Public  Property enumList() as PicklistEnum
	Get
		Return _enumList
	End Get
	Set(value As PicklistEnum)
		_enumList = value
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,viewIfNoAccess as Boolean,enumList as PicklistEnum)
		MyBase.New(name,accessLevel,viewIfNoAccess)
		_enumList = enumList
	End Sub
End Class

Public Class PickListEnum
	

End Class
Public Class Group

Private  _name as String
Public  Property name() as String
	Get
		Return _name
	End Get
	Set(value As String)
		_name = value
	End Set
End Property
Private  _accessLevel as Int32
Public  Property accessLevel() as Int32
	Get
		Return _accessLevel
	End Get
	Set(value As Int32)
		_accessLevel = value
	End Set
End Property
Private  _settingsList as Setting()
Public  Property settingsList() as Setting()
	Get
		Return _settingsList
	End Get
	Set(value As Setting())
		_settingsList = value
	End Set
End Property
	Public Sub New(name as String,accessLevel as Int32,settingsList as Setting())
		_name = name
		_accessLevel = accessLevel
		_settingsList = settingsList
	End Sub
End Class

Public Class clsSettings
Private Shared _tester as Picklist = New Picklist("tester",0,True,testerEnum)
Public Shared Property tester() as Picklist
	Get
		Return _tester
	End Get
	Set(value As Picklist)
		_tester = value
	End Set
End Property
Public Class testerEnum
	Inherits PickListEnum

Private Shared _firstItem as Int32 = 23
Public Shared Property firstItem() as Int32
	Get
		Return _firstItem
	End Get
	Set(value As Int32)
		_firstItem = value
	End Set
End Property
End Class
Private Shared _general as Group = New Group("general",1,{tester})
Public Shared Property general() as Group
	Get
		Return _general
	End Get
	Set(value As Group)
		_general = value
	End Set
End Property
Private Shared _groupsList as Group() = {general}
Public Shared Property groupsList() as Group()
	Get
		Return _groupsList
	End Get
	Set(value As Group())
		_groupsList = value
	End Set
End Property
End Class
