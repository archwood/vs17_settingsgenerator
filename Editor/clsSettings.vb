Imports clsSettingsManager
Public Class clsSettings
	Public Enum opts_SaveVehicleWt
		never = 0
		auto = 1
		prompt = 2
	End Enum
	Private Shared _SaveVehicleWt_never As PicklistOption = New PicklistOption("never", "Never", 0)
	Public Shared Property SaveVehicleWt_never() As PicklistOption
		Get
			Return _SaveVehicleWt_never
		End Get
		Set(value As PicklistOption)
			_SaveVehicleWt_never = value
		End Set
	End Property
	Private Shared _SaveVehicleWt_auto As PicklistOption = New PicklistOption("auto", "Automatic", 1)
	Public Shared Property SaveVehicleWt_auto() As PicklistOption
		Get
			Return _SaveVehicleWt_auto
		End Get
		Set(value As PicklistOption)
			_SaveVehicleWt_auto = value
		End Set
	End Property
	Private Shared _SaveVehicleWt_prompt As PicklistOption = New PicklistOption("prompt", "Prompt", 2)
	Public Shared Property SaveVehicleWt_prompt() As PicklistOption
		Get
			Return _SaveVehicleWt_prompt
		End Get
		Set(value As PicklistOption)
			_SaveVehicleWt_prompt = value
		End Set
	End Property
	Private Shared _SaveVehicleWt As Picklist = New Picklist("SaveVehicleWt", "Save new vehicle weight?", 1, True, 0, {SaveVehicleWt_never, SaveVehicleWt_auto, SaveVehicleWt_prompt})
	Public Shared Property SaveVehicleWt() As Picklist
		Get
			Return _SaveVehicleWt
		End Get
		Set(value As Picklist)
			_SaveVehicleWt = value
		End Set
	End Property
	Public Enum opts_UpdateVehicleWt
		never = 0
		auto = 1
		prompt = 2
	End Enum
	Private Shared _UpdateVehicleWt_never As PicklistOption = New PicklistOption("never", "Never", 0)
	Public Shared Property UpdateVehicleWt_never() As PicklistOption
		Get
			Return _UpdateVehicleWt_never
		End Get
		Set(value As PicklistOption)
			_UpdateVehicleWt_never = value
		End Set
	End Property
	Private Shared _UpdateVehicleWt_auto As PicklistOption = New PicklistOption("auto", "Automatic", 1)
	Public Shared Property UpdateVehicleWt_auto() As PicklistOption
		Get
			Return _UpdateVehicleWt_auto
		End Get
		Set(value As PicklistOption)
			_UpdateVehicleWt_auto = value
		End Set
	End Property
	Private Shared _UpdateVehicleWt_prompt As PicklistOption = New PicklistOption("prompt", "Prompt", 2)
	Public Shared Property UpdateVehicleWt_prompt() As PicklistOption
		Get
			Return _UpdateVehicleWt_prompt
		End Get
		Set(value As PicklistOption)
			_UpdateVehicleWt_prompt = value
		End Set
	End Property
	Private Shared _UpdateVehicleWt As Picklist = New Picklist("UpdateVehicleWt", "Update vehicle weight?", 1, True, 0, {UpdateVehicleWt_never, UpdateVehicleWt_auto, UpdateVehicleWt_prompt})
	Public Shared Property UpdateVehicleWt() As Picklist
		Get
			Return _UpdateVehicleWt
		End Get
		Set(value As Picklist)
			_UpdateVehicleWt = value
		End Set
	End Property
	Public Enum opts_PrintTicket
		never = 0
		auto = 1
		prompt = 2
	End Enum
	Private Shared _PrintTicket_never As PicklistOption = New PicklistOption("never", "Never", 0)
	Public Shared Property PrintTicket_never() As PicklistOption
		Get
			Return _PrintTicket_never
		End Get
		Set(value As PicklistOption)
			_PrintTicket_never = value
		End Set
	End Property
	Private Shared _PrintTicket_auto As PicklistOption = New PicklistOption("auto", "Automatic", 1)
	Public Shared Property PrintTicket_auto() As PicklistOption
		Get
			Return _PrintTicket_auto
		End Get
		Set(value As PicklistOption)
			_PrintTicket_auto = value
		End Set
	End Property
	Private Shared _PrintTicket_prompt As PicklistOption = New PicklistOption("prompt", "Prompt", 2)
	Public Shared Property PrintTicket_prompt() As PicklistOption
		Get
			Return _PrintTicket_prompt
		End Get
		Set(value As PicklistOption)
			_PrintTicket_prompt = value
		End Set
	End Property
	Private Shared _PrintTicket As Picklist = New Picklist("PrintTicket", "Print ticket on 2nd weight?", 1, True, 2, {PrintTicket_never, PrintTicket_auto, PrintTicket_prompt})
	Public Shared Property PrintTicket() As Picklist
		Get
			Return _PrintTicket
		End Get
		Set(value As Picklist)
			_PrintTicket = value
		End Set
	End Property
	Public Enum opts_InfoEntry
		never = 0
		firstOnly = 1
		secondOnly = 2
		both = 3
	End Enum
	Private Shared _InfoEntry_never As PicklistOption = New PicklistOption("never", "Never", 0)
	Public Shared Property InfoEntry_never() As PicklistOption
		Get
			Return _InfoEntry_never
		End Get
		Set(value As PicklistOption)
			_InfoEntry_never = value
		End Set
	End Property
	Private Shared _InfoEntry_firstOnly As PicklistOption = New PicklistOption("firstOnly", "1st weight only", 1)
	Public Shared Property InfoEntry_firstOnly() As PicklistOption
		Get
			Return _InfoEntry_firstOnly
		End Get
		Set(value As PicklistOption)
			_InfoEntry_firstOnly = value
		End Set
	End Property
	Private Shared _InfoEntry_secondOnly As PicklistOption = New PicklistOption("secondOnly", "2nd weight only", 2)
	Public Shared Property InfoEntry_secondOnly() As PicklistOption
		Get
			Return _InfoEntry_secondOnly
		End Get
		Set(value As PicklistOption)
			_InfoEntry_secondOnly = value
		End Set
	End Property
	Private Shared _InfoEntry_both As PicklistOption = New PicklistOption("both", "Both 1st and 2nd weight", 3)
	Public Shared Property InfoEntry_both() As PicklistOption
		Get
			Return _InfoEntry_both
		End Get
		Set(value As PicklistOption)
			_InfoEntry_both = value
		End Set
	End Property
	Private Shared _InfoEntry As Picklist = New Picklist("InfoEntry", "Information Entry", 1, True, 3, {InfoEntry_never, InfoEntry_firstOnly, InfoEntry_secondOnly, InfoEntry_both})
	Public Shared Property InfoEntry() As Picklist
		Get
			Return _InfoEntry
		End Get
		Set(value As Picklist)
			_InfoEntry = value
		End Set
	End Property
	Private Shared _AutoLogoffMins As Numeric = New Numeric("AutoLogoffMins", "Auto logoff (mins)", 1, True, 0, 0, 60, 0)
	Public Shared Property AutoLogoffMins() As Numeric
		Get
			Return _AutoLogoffMins
		End Get
		Set(value As Numeric)
			_AutoLogoffMins = value
		End Set
	End Property
	Private Shared _ShowWaiting As Binary = New Binary("ShowWaiting", "Waiting vehicles", 1, True, False)
	Public Shared Property ShowWaiting() As Binary
		Get
			Return _ShowWaiting
		End Get
		Set(value As Binary)
			_ShowWaiting = value
		End Set
	End Property
	Public Enum opts_MvmtDetermination
		none = 0
		op = 1
		onsite = 2
		offsite = 3
	End Enum
	Private Shared _MvmtDetermination_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property MvmtDetermination_none() As PicklistOption
		Get
			Return _MvmtDetermination_none
		End Get
		Set(value As PicklistOption)
			_MvmtDetermination_none = value
		End Set
	End Property
	Private Shared _MvmtDetermination_op As PicklistOption = New PicklistOption("op", "Operator specified", 1)
	Public Shared Property MvmtDetermination_op() As PicklistOption
		Get
			Return _MvmtDetermination_op
		End Get
		Set(value As PicklistOption)
			_MvmtDetermination_op = value
		End Set
	End Property
	Private Shared _MvmtDetermination_onsite As PicklistOption = New PicklistOption("onsite", "On site vehicles assumed", 2)
	Public Shared Property MvmtDetermination_onsite() As PicklistOption
		Get
			Return _MvmtDetermination_onsite
		End Get
		Set(value As PicklistOption)
			_MvmtDetermination_onsite = value
		End Set
	End Property
	Private Shared _MvmtDetermination_offsite As PicklistOption = New PicklistOption("offsite", "Off site vehicles assumed", 3)
	Public Shared Property MvmtDetermination_offsite() As PicklistOption
		Get
			Return _MvmtDetermination_offsite
		End Get
		Set(value As PicklistOption)
			_MvmtDetermination_offsite = value
		End Set
	End Property
	Private Shared _MvmtDetermination As Picklist = New Picklist("MvmtDetermination", "Goods in/out determination", 1, True, 3, {MvmtDetermination_none, MvmtDetermination_op, MvmtDetermination_onsite, MvmtDetermination_offsite})
	Public Shared Property MvmtDetermination() As Picklist
		Get
			Return _MvmtDetermination
		End Get
		Set(value As Picklist)
			_MvmtDetermination = value
		End Set
	End Property
	Private Shared _MvmtOverride As Binary = New Binary("MvmtOverride", "Allow override goods in/out?", 1, True, True)
	Public Shared Property MvmtOverride() As Binary
		Get
			Return _MvmtOverride
		End Get
		Set(value As Binary)
			_MvmtOverride = value
		End Set
	End Property
	Public Enum opts_DefaultMvmt
		none = 0
		goodsIn = 1
		goodsOut = 2
	End Enum
	Private Shared _DefaultMvmt_none As PicklistOption = New PicklistOption("none", "None Specified", 0)
	Public Shared Property DefaultMvmt_none() As PicklistOption
		Get
			Return _DefaultMvmt_none
		End Get
		Set(value As PicklistOption)
			_DefaultMvmt_none = value
		End Set
	End Property
	Private Shared _DefaultMvmt_goodsIn As PicklistOption = New PicklistOption("goodsIn", "Goods in", 1)
	Public Shared Property DefaultMvmt_goodsIn() As PicklistOption
		Get
			Return _DefaultMvmt_goodsIn
		End Get
		Set(value As PicklistOption)
			_DefaultMvmt_goodsIn = value
		End Set
	End Property
	Private Shared _DefaultMvmt_goodsOut As PicklistOption = New PicklistOption("goodsOut", "Goods out", 2)
	Public Shared Property DefaultMvmt_goodsOut() As PicklistOption
		Get
			Return _DefaultMvmt_goodsOut
		End Get
		Set(value As PicklistOption)
			_DefaultMvmt_goodsOut = value
		End Set
	End Property
	Private Shared _DefaultMvmt As Picklist = New Picklist("DefaultMvmt", "Default determination", 1, True, 0, {DefaultMvmt_none, DefaultMvmt_goodsIn, DefaultMvmt_goodsOut})
	Public Shared Property DefaultMvmt() As Picklist
		Get
			Return _DefaultMvmt
		End Get
		Set(value As Picklist)
			_DefaultMvmt = value
		End Set
	End Property
	Private Shared _CheckAdvisedWt As Binary = New Binary("CheckAdvisedWt", "Check advised wieght", 1, True, True)
	Public Shared Property CheckAdvisedWt() As Binary
		Get
			Return _CheckAdvisedWt
		End Get
		Set(value As Binary)
			_CheckAdvisedWt = value
		End Set
	End Property
	Private Shared _AdvisedWtTolerance As Numeric = New Numeric("AdvisedWtTolerance", "Tolerance", 1, True, 500, 0, 10000, 0)
	Public Shared Property AdvisedWtTolerance() As Numeric
		Get
			Return _AdvisedWtTolerance
		End Get
		Set(value As Numeric)
			_AdvisedWtTolerance = value
		End Set
	End Property
	Private Shared _FileSaveLocation As PathLocation = New PathLocation("FileSaveLocation", "File location", 1, True, "", False)
	Public Shared Property FileSaveLocation() As PathLocation
		Get
			Return _FileSaveLocation
		End Get
		Set(value As PathLocation)
			_FileSaveLocation = value
		End Set
	End Property
	Private Shared _general As Group = New Group("general", "General", 1, {SaveVehicleWt, UpdateVehicleWt, PrintTicket, InfoEntry, AutoLogoffMins, ShowWaiting, MvmtDetermination, MvmtOverride, DefaultMvmt, CheckAdvisedWt, AdvisedWtTolerance, FileSaveLocation})
	Public Shared Property general() As Group
		Get
			Return _general
		End Get
		Set(value As Group)
			_general = value
		End Set
	End Property
	Public Enum opts_NumWeighbridges
		none = 0
		one = 1
		two = 2
	End Enum
	Private Shared _NumWeighbridges_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property NumWeighbridges_none() As PicklistOption
		Get
			Return _NumWeighbridges_none
		End Get
		Set(value As PicklistOption)
			_NumWeighbridges_none = value
		End Set
	End Property
	Private Shared _NumWeighbridges_one As PicklistOption = New PicklistOption("one", "One", 1)
	Public Shared Property NumWeighbridges_one() As PicklistOption
		Get
			Return _NumWeighbridges_one
		End Get
		Set(value As PicklistOption)
			_NumWeighbridges_one = value
		End Set
	End Property
	Private Shared _NumWeighbridges_two As PicklistOption = New PicklistOption("two", "Two", 2)
	Public Shared Property NumWeighbridges_two() As PicklistOption
		Get
			Return _NumWeighbridges_two
		End Get
		Set(value As PicklistOption)
			_NumWeighbridges_two = value
		End Set
	End Property
	Private Shared _NumWeighbridges As Picklist = New Picklist("NumWeighbridges", "Number of weighbridges", 1, True, 1, {NumWeighbridges_none, NumWeighbridges_one, NumWeighbridges_two})
	Public Shared Property NumWeighbridges() As Picklist
		Get
			Return _NumWeighbridges
		End Get
		Set(value As Picklist)
			_NumWeighbridges = value
		End Set
	End Property
	Private Shared _DefaultWeighbridge As Numeric = New Numeric("DefaultWeighbridge", "Default weighbridge", 1, True, 1, 0, 2, 0)
	Public Shared Property DefaultWeighbridge() As Numeric
		Get
			Return _DefaultWeighbridge
		End Get
		Set(value As Numeric)
			_DefaultWeighbridge = value
		End Set
	End Property
	Private Shared _ScaleFormat As Text = New Text("ScaleFormat", "Display format", 1, True, "0", 8)
	Public Shared Property ScaleFormat() As Text
		Get
			Return _ScaleFormat
		End Get
		Set(value As Text)
			_ScaleFormat = value
		End Set
	End Property
	Private Shared _WB1Name As Text = New Text("WB1Name", "WB1 name", 1, True, "0", 32)
	Public Shared Property WB1Name() As Text
		Get
			Return _WB1Name
		End Get
		Set(value As Text)
			_WB1Name = value
		End Set
	End Property
	Public Enum opts_WB1ScaleType
		systecCom = 0
		systecDirect = 1
		rinstrumB = 2
		rinstrumA = 3
		masterK = 4
		whitebird = 5
		tScale = 6
		rinstrumC = 7
		weightron = 8
	End Enum
	Private Shared _WB1ScaleType_systecCom As PicklistOption = New PicklistOption("systecCom", "SysTec Com", 0)
	Public Shared Property WB1ScaleType_systecCom() As PicklistOption
		Get
			Return _WB1ScaleType_systecCom
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_systecCom = value
		End Set
	End Property
	Private Shared _WB1ScaleType_systecDirect As PicklistOption = New PicklistOption("systecDirect", "SysTec Direct", 1)
	Public Shared Property WB1ScaleType_systecDirect() As PicklistOption
		Get
			Return _WB1ScaleType_systecDirect
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_systecDirect = value
		End Set
	End Property
	Private Shared _WB1ScaleType_rinstrumB As PicklistOption = New PicklistOption("rinstrumB", "Rinstrum protocol B", 2)
	Public Shared Property WB1ScaleType_rinstrumB() As PicklistOption
		Get
			Return _WB1ScaleType_rinstrumB
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_rinstrumB = value
		End Set
	End Property
	Private Shared _WB1ScaleType_rinstrumA As PicklistOption = New PicklistOption("rinstrumA", "Rinstrum protocol A", 3)
	Public Shared Property WB1ScaleType_rinstrumA() As PicklistOption
		Get
			Return _WB1ScaleType_rinstrumA
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_rinstrumA = value
		End Set
	End Property
	Private Shared _WB1ScaleType_masterK As PicklistOption = New PicklistOption("masterK", "Master K", 4)
	Public Shared Property WB1ScaleType_masterK() As PicklistOption
		Get
			Return _WB1ScaleType_masterK
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_masterK = value
		End Set
	End Property
	Private Shared _WB1ScaleType_whitebird As PicklistOption = New PicklistOption("whitebird", "Whitebird", 5)
	Public Shared Property WB1ScaleType_whitebird() As PicklistOption
		Get
			Return _WB1ScaleType_whitebird
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_whitebird = value
		End Set
	End Property
	Private Shared _WB1ScaleType_tScale As PicklistOption = New PicklistOption("tScale", "TScale U8", 6)
	Public Shared Property WB1ScaleType_tScale() As PicklistOption
		Get
			Return _WB1ScaleType_tScale
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_tScale = value
		End Set
	End Property
	Private Shared _WB1ScaleType_rinstrumC As PicklistOption = New PicklistOption("rinstrumC", "Rinstrum protocol C", 7)
	Public Shared Property WB1ScaleType_rinstrumC() As PicklistOption
		Get
			Return _WB1ScaleType_rinstrumC
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_rinstrumC = value
		End Set
	End Property
	Private Shared _WB1ScaleType_weightron As PicklistOption = New PicklistOption("weightron", "WeightonD410", 8)
	Public Shared Property WB1ScaleType_weightron() As PicklistOption
		Get
			Return _WB1ScaleType_weightron
		End Get
		Set(value As PicklistOption)
			_WB1ScaleType_weightron = value
		End Set
	End Property
	Private Shared _WB1ScaleType As Picklist = New Picklist("WB1ScaleType", "WB1 scale type", 1, True, 1, {WB1ScaleType_systecCom, WB1ScaleType_systecDirect, WB1ScaleType_rinstrumB, WB1ScaleType_rinstrumA, WB1ScaleType_masterK, WB1ScaleType_whitebird, WB1ScaleType_tScale, WB1ScaleType_rinstrumC, WB1ScaleType_weightron})
	Public Shared Property WB1ScaleType() As Picklist
		Get
			Return _WB1ScaleType
		End Get
		Set(value As Picklist)
			_WB1ScaleType = value
		End Set
	End Property
	Public Enum opts_WB1ScaleConnection
		serial = 0
		ethernet = 1
		systec = 2
	End Enum
	Private Shared _WB1ScaleConnection_serial As PicklistOption = New PicklistOption("serial", "Serial", 0)
	Public Shared Property WB1ScaleConnection_serial() As PicklistOption
		Get
			Return _WB1ScaleConnection_serial
		End Get
		Set(value As PicklistOption)
			_WB1ScaleConnection_serial = value
		End Set
	End Property
	Private Shared _WB1ScaleConnection_ethernet As PicklistOption = New PicklistOption("ethernet", "Ethernet", 1)
	Public Shared Property WB1ScaleConnection_ethernet() As PicklistOption
		Get
			Return _WB1ScaleConnection_ethernet
		End Get
		Set(value As PicklistOption)
			_WB1ScaleConnection_ethernet = value
		End Set
	End Property
	Private Shared _WB1ScaleConnection_systec As PicklistOption = New PicklistOption("systec", "SysTec com", 2)
	Public Shared Property WB1ScaleConnection_systec() As PicklistOption
		Get
			Return _WB1ScaleConnection_systec
		End Get
		Set(value As PicklistOption)
			_WB1ScaleConnection_systec = value
		End Set
	End Property
	Private Shared _WB1ScaleConnection As Picklist = New Picklist("WB1ScaleConnection", "WB1 interface", 1, True, 1, {WB1ScaleConnection_serial, WB1ScaleConnection_ethernet, WB1ScaleConnection_systec})
	Public Shared Property WB1ScaleConnection() As Picklist
		Get
			Return _WB1ScaleConnection
		End Get
		Set(value As Picklist)
			_WB1ScaleConnection = value
		End Set
	End Property
	Private Shared _WB1IPAddress As Text = New Text("WB1IPAddress", "WB1 IP address", 1, True, "127.0.0.1", 15)
	Public Shared Property WB1IPAddress() As Text
		Get
			Return _WB1IPAddress
		End Get
		Set(value As Text)
			_WB1IPAddress = value
		End Set
	End Property
	Private Shared _WB1IPPort As Numeric = New Numeric("WB1IPPort", "WB1 IP port", 1, True, 1234, 0, 65535, 1)
	Public Shared Property WB1IPPort() As Numeric
		Get
			Return _WB1IPPort
		End Get
		Set(value As Numeric)
			_WB1IPPort = value
		End Set
	End Property
	Private Shared _WB1ComPort As ComPortList = New ComPortList("WB1ComPort", "WB1 com port", 1, True, 1)
	Public Shared Property WB1ComPort() As ComPortList
		Get
			Return _WB1ComPort
		End Get
		Set(value As ComPortList)
			_WB1ComPort = value
		End Set
	End Property
	Private Shared _WB1Baud As Numeric = New Numeric("WB1Baud", "WB1 baud rate", 1, True, 9600, 0, 1152000, 100)
	Public Shared Property WB1Baud() As Numeric
		Get
			Return _WB1Baud
		End Get
		Set(value As Numeric)
			_WB1Baud = value
		End Set
	End Property
	Private Shared _WB1DataBits As Numeric = New Numeric("WB1DataBits", "WB1 data bits", 1, True, 8, 0, 8, 7)
	Public Shared Property WB1DataBits() As Numeric
		Get
			Return _WB1DataBits
		End Get
		Set(value As Numeric)
			_WB1DataBits = value
		End Set
	End Property
	Public Enum opts_WB1Parity
		none = 0
		odd = 1
		even = 2
		mark = 3
		space = 4
	End Enum
	Private Shared _WB1Parity_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property WB1Parity_none() As PicklistOption
		Get
			Return _WB1Parity_none
		End Get
		Set(value As PicklistOption)
			_WB1Parity_none = value
		End Set
	End Property
	Private Shared _WB1Parity_odd As PicklistOption = New PicklistOption("odd", "Odd", 1)
	Public Shared Property WB1Parity_odd() As PicklistOption
		Get
			Return _WB1Parity_odd
		End Get
		Set(value As PicklistOption)
			_WB1Parity_odd = value
		End Set
	End Property
	Private Shared _WB1Parity_even As PicklistOption = New PicklistOption("even", "Even", 2)
	Public Shared Property WB1Parity_even() As PicklistOption
		Get
			Return _WB1Parity_even
		End Get
		Set(value As PicklistOption)
			_WB1Parity_even = value
		End Set
	End Property
	Private Shared _WB1Parity_mark As PicklistOption = New PicklistOption("mark", "Mark", 3)
	Public Shared Property WB1Parity_mark() As PicklistOption
		Get
			Return _WB1Parity_mark
		End Get
		Set(value As PicklistOption)
			_WB1Parity_mark = value
		End Set
	End Property
	Private Shared _WB1Parity_space As PicklistOption = New PicklistOption("space", "Space", 4)
	Public Shared Property WB1Parity_space() As PicklistOption
		Get
			Return _WB1Parity_space
		End Get
		Set(value As PicklistOption)
			_WB1Parity_space = value
		End Set
	End Property
	Private Shared _WB1Parity As Picklist = New Picklist("WB1Parity", "WB1 parity", 1, True, 0, {WB1Parity_none, WB1Parity_odd, WB1Parity_even, WB1Parity_mark, WB1Parity_space})
	Public Shared Property WB1Parity() As Picklist
		Get
			Return _WB1Parity
		End Get
		Set(value As Picklist)
			_WB1Parity = value
		End Set
	End Property
	Private Shared _WB1StopBits As Numeric = New Numeric("WB1StopBits", "WB1 stop bits", 1, True, 1, 1, 2, 0)
	Public Shared Property WB1StopBits() As Numeric
		Get
			Return _WB1StopBits
		End Get
		Set(value As Numeric)
			_WB1StopBits = value
		End Set
	End Property
	Private Shared _WB1Settings As Subgroup = New Subgroup("WB1Settings", "WB1 Settings", 1, {WB1Name, WB1ScaleType, WB1ScaleConnection, WB1IPAddress, WB1IPPort, WB1ComPort, WB1Baud, WB1DataBits, WB1Parity, WB1StopBits})
	Public Shared Property WB1Settings() As Subgroup
		Get
			Return _WB1Settings
		End Get
		Set(value As Subgroup)
			_WB1Settings = value
		End Set
	End Property
	Private Shared _WB2Name As Text = New Text("WB2Name", "WB2 name", 1, True, "0", 32)
	Public Shared Property WB2Name() As Text
		Get
			Return _WB2Name
		End Get
		Set(value As Text)
			_WB2Name = value
		End Set
	End Property
	Public Enum opts_WB2ScaleType
		systecCom = 0
		systecDirect = 1
		rinstrumB = 2
		rinstrumA = 3
		masterK = 4
		whitebird = 5
		tScale = 6
		rinstrumC = 7
		weightron = 8
	End Enum
	Private Shared _WB2ScaleType_systecCom As PicklistOption = New PicklistOption("systecCom", "SysTec Com", 0)
	Public Shared Property WB2ScaleType_systecCom() As PicklistOption
		Get
			Return _WB2ScaleType_systecCom
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_systecCom = value
		End Set
	End Property
	Private Shared _WB2ScaleType_systecDirect As PicklistOption = New PicklistOption("systecDirect", "SysTec Direct", 1)
	Public Shared Property WB2ScaleType_systecDirect() As PicklistOption
		Get
			Return _WB2ScaleType_systecDirect
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_systecDirect = value
		End Set
	End Property
	Private Shared _WB2ScaleType_rinstrumB As PicklistOption = New PicklistOption("rinstrumB", "Rinstrum protocol B", 2)
	Public Shared Property WB2ScaleType_rinstrumB() As PicklistOption
		Get
			Return _WB2ScaleType_rinstrumB
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_rinstrumB = value
		End Set
	End Property
	Private Shared _WB2ScaleType_rinstrumA As PicklistOption = New PicklistOption("rinstrumA", "Rinstrum protocol A", 3)
	Public Shared Property WB2ScaleType_rinstrumA() As PicklistOption
		Get
			Return _WB2ScaleType_rinstrumA
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_rinstrumA = value
		End Set
	End Property
	Private Shared _WB2ScaleType_masterK As PicklistOption = New PicklistOption("masterK", "Master K", 4)
	Public Shared Property WB2ScaleType_masterK() As PicklistOption
		Get
			Return _WB2ScaleType_masterK
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_masterK = value
		End Set
	End Property
	Private Shared _WB2ScaleType_whitebird As PicklistOption = New PicklistOption("whitebird", "Whitebird", 5)
	Public Shared Property WB2ScaleType_whitebird() As PicklistOption
		Get
			Return _WB2ScaleType_whitebird
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_whitebird = value
		End Set
	End Property
	Private Shared _WB2ScaleType_tScale As PicklistOption = New PicklistOption("tScale", "TScale U8", 6)
	Public Shared Property WB2ScaleType_tScale() As PicklistOption
		Get
			Return _WB2ScaleType_tScale
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_tScale = value
		End Set
	End Property
	Private Shared _WB2ScaleType_rinstrumC As PicklistOption = New PicklistOption("rinstrumC", "Rinstrum protocol C", 7)
	Public Shared Property WB2ScaleType_rinstrumC() As PicklistOption
		Get
			Return _WB2ScaleType_rinstrumC
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_rinstrumC = value
		End Set
	End Property
	Private Shared _WB2ScaleType_weightron As PicklistOption = New PicklistOption("weightron", "WeightonD410", 8)
	Public Shared Property WB2ScaleType_weightron() As PicklistOption
		Get
			Return _WB2ScaleType_weightron
		End Get
		Set(value As PicklistOption)
			_WB2ScaleType_weightron = value
		End Set
	End Property
	Private Shared _WB2ScaleType As Picklist = New Picklist("WB2ScaleType", "WB2 scale type", 1, True, 1, {WB2ScaleType_systecCom, WB2ScaleType_systecDirect, WB2ScaleType_rinstrumB, WB2ScaleType_rinstrumA, WB2ScaleType_masterK, WB2ScaleType_whitebird, WB2ScaleType_tScale, WB2ScaleType_rinstrumC, WB2ScaleType_weightron})
	Public Shared Property WB2ScaleType() As Picklist
		Get
			Return _WB2ScaleType
		End Get
		Set(value As Picklist)
			_WB2ScaleType = value
		End Set
	End Property
	Public Enum opts_WB2ScaleConnection
		serial = 0
		ethernet = 1
		systec = 2
	End Enum
	Private Shared _WB2ScaleConnection_serial As PicklistOption = New PicklistOption("serial", "Serial", 0)
	Public Shared Property WB2ScaleConnection_serial() As PicklistOption
		Get
			Return _WB2ScaleConnection_serial
		End Get
		Set(value As PicklistOption)
			_WB2ScaleConnection_serial = value
		End Set
	End Property
	Private Shared _WB2ScaleConnection_ethernet As PicklistOption = New PicklistOption("ethernet", "Ethernet", 1)
	Public Shared Property WB2ScaleConnection_ethernet() As PicklistOption
		Get
			Return _WB2ScaleConnection_ethernet
		End Get
		Set(value As PicklistOption)
			_WB2ScaleConnection_ethernet = value
		End Set
	End Property
	Private Shared _WB2ScaleConnection_systec As PicklistOption = New PicklistOption("systec", "SysTec com", 2)
	Public Shared Property WB2ScaleConnection_systec() As PicklistOption
		Get
			Return _WB2ScaleConnection_systec
		End Get
		Set(value As PicklistOption)
			_WB2ScaleConnection_systec = value
		End Set
	End Property
	Private Shared _WB2ScaleConnection As Picklist = New Picklist("WB2ScaleConnection", "WB2 interface", 1, True, 1, {WB2ScaleConnection_serial, WB2ScaleConnection_ethernet, WB2ScaleConnection_systec})
	Public Shared Property WB2ScaleConnection() As Picklist
		Get
			Return _WB2ScaleConnection
		End Get
		Set(value As Picklist)
			_WB2ScaleConnection = value
		End Set
	End Property
	Private Shared _WB2IPAddress As Text = New Text("WB2IPAddress", "WB2 IP address", 1, True, "127.0.0.1", 15)
	Public Shared Property WB2IPAddress() As Text
		Get
			Return _WB2IPAddress
		End Get
		Set(value As Text)
			_WB2IPAddress = value
		End Set
	End Property
	Private Shared _WB2IPPort As Numeric = New Numeric("WB2IPPort", "WB2 IP port", 1, True, 1234, 0, 65535, 1)
	Public Shared Property WB2IPPort() As Numeric
		Get
			Return _WB2IPPort
		End Get
		Set(value As Numeric)
			_WB2IPPort = value
		End Set
	End Property
	Private Shared _WB2ComPort As ComPortList = New ComPortList("WB2ComPort", "WB2 com port", 1, True, 1)
	Public Shared Property WB2ComPort() As ComPortList
		Get
			Return _WB2ComPort
		End Get
		Set(value As ComPortList)
			_WB2ComPort = value
		End Set
	End Property
	Private Shared _WB2Baud As Numeric = New Numeric("WB2Baud", "WB2 baud rate", 1, True, 9600, 0, 1152000, 100)
	Public Shared Property WB2Baud() As Numeric
		Get
			Return _WB2Baud
		End Get
		Set(value As Numeric)
			_WB2Baud = value
		End Set
	End Property
	Private Shared _WB2DataBits As Numeric = New Numeric("WB2DataBits", "WB2 data bits", 1, True, 8, 0, 8, 7)
	Public Shared Property WB2DataBits() As Numeric
		Get
			Return _WB2DataBits
		End Get
		Set(value As Numeric)
			_WB2DataBits = value
		End Set
	End Property
	Public Enum opts_WB2Parity
		none = 0
		odd = 1
		even = 2
		mark = 3
		space = 4
	End Enum
	Private Shared _WB2Parity_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property WB2Parity_none() As PicklistOption
		Get
			Return _WB2Parity_none
		End Get
		Set(value As PicklistOption)
			_WB2Parity_none = value
		End Set
	End Property
	Private Shared _WB2Parity_odd As PicklistOption = New PicklistOption("odd", "Odd", 1)
	Public Shared Property WB2Parity_odd() As PicklistOption
		Get
			Return _WB2Parity_odd
		End Get
		Set(value As PicklistOption)
			_WB2Parity_odd = value
		End Set
	End Property
	Private Shared _WB2Parity_even As PicklistOption = New PicklistOption("even", "Even", 2)
	Public Shared Property WB2Parity_even() As PicklistOption
		Get
			Return _WB2Parity_even
		End Get
		Set(value As PicklistOption)
			_WB2Parity_even = value
		End Set
	End Property
	Private Shared _WB2Parity_mark As PicklistOption = New PicklistOption("mark", "Mark", 3)
	Public Shared Property WB2Parity_mark() As PicklistOption
		Get
			Return _WB2Parity_mark
		End Get
		Set(value As PicklistOption)
			_WB2Parity_mark = value
		End Set
	End Property
	Private Shared _WB2Parity_space As PicklistOption = New PicklistOption("space", "Space", 4)
	Public Shared Property WB2Parity_space() As PicklistOption
		Get
			Return _WB2Parity_space
		End Get
		Set(value As PicklistOption)
			_WB2Parity_space = value
		End Set
	End Property
	Private Shared _WB2Parity As Picklist = New Picklist("WB2Parity", "WB2 parity", 1, True, 0, {WB2Parity_none, WB2Parity_odd, WB2Parity_even, WB2Parity_mark, WB2Parity_space})
	Public Shared Property WB2Parity() As Picklist
		Get
			Return _WB2Parity
		End Get
		Set(value As Picklist)
			_WB2Parity = value
		End Set
	End Property
	Private Shared _WB2StopBits As Numeric = New Numeric("WB2StopBits", "WB2 stop bits", 1, True, 1, 1, 2, 0)
	Public Shared Property WB2StopBits() As Numeric
		Get
			Return _WB2StopBits
		End Get
		Set(value As Numeric)
			_WB2StopBits = value
		End Set
	End Property
	Private Shared _WB2Settings As Subgroup = New Subgroup("WB2Settings", "WB2 Settings", 1, {WB2Name, WB2ScaleType, WB2ScaleConnection, WB2IPAddress, WB2IPPort, WB2ComPort, WB2Baud, WB2DataBits, WB2Parity, WB2StopBits})
	Public Shared Property WB2Settings() As Subgroup
		Get
			Return _WB2Settings
		End Get
		Set(value As Subgroup)
			_WB2Settings = value
		End Set
	End Property
	Private Shared _weighbridges As Group = New Group("weighbridges", "Weighbridges", 1, {NumWeighbridges, DefaultWeighbridge, ScaleFormat, WB1Settings, WB2Settings})
	Public Shared Property weighbridges() As Group
		Get
			Return _weighbridges
		End Get
		Set(value As Group)
			_weighbridges = value
		End Set
	End Property
	Private Shared _WindowsPrinter As Binary = New Binary("WindowsPrinter", "Windows printer", 1, True, True)
	Public Shared Property WindowsPrinter() As Binary
		Get
			Return _WindowsPrinter
		End Get
		Set(value As Binary)
			_WindowsPrinter = value
		End Set
	End Property
	Private Shared _TicketCopies As Numeric = New Numeric("TicketCopies", "Number of ticket copies", 1, True, 0, 0, 1000, 0)
	Public Shared Property TicketCopies() As Numeric
		Get
			Return _TicketCopies
		End Get
		Set(value As Numeric)
			_TicketCopies = value
		End Set
	End Property
	Public Enum opts_OutputType
		none = 0
		singleLights = 1
		dualLights = 2
		barriers = 3
	End Enum
	Private Shared _OutputType_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property OutputType_none() As PicklistOption
		Get
			Return _OutputType_none
		End Get
		Set(value As PicklistOption)
			_OutputType_none = value
		End Set
	End Property
	Private Shared _OutputType_singleLights As PicklistOption = New PicklistOption("singleLights", "Single Lights", 1)
	Public Shared Property OutputType_singleLights() As PicklistOption
		Get
			Return _OutputType_singleLights
		End Get
		Set(value As PicklistOption)
			_OutputType_singleLights = value
		End Set
	End Property
	Private Shared _OutputType_dualLights As PicklistOption = New PicklistOption("dualLights", "Dual Lights", 2)
	Public Shared Property OutputType_dualLights() As PicklistOption
		Get
			Return _OutputType_dualLights
		End Get
		Set(value As PicklistOption)
			_OutputType_dualLights = value
		End Set
	End Property
	Private Shared _OutputType_barriers As PicklistOption = New PicklistOption("barriers", "Barriers", 3)
	Public Shared Property OutputType_barriers() As PicklistOption
		Get
			Return _OutputType_barriers
		End Get
		Set(value As PicklistOption)
			_OutputType_barriers = value
		End Set
	End Property
	Private Shared _OutputType As Picklist = New Picklist("OutputType", "Remote output type", 1, True, 2, {OutputType_none, OutputType_singleLights, OutputType_dualLights, OutputType_barriers})
	Public Shared Property OutputType() As Picklist
		Get
			Return _OutputType
		End Get
		Set(value As Picklist)
			_OutputType = value
		End Set
	End Property
	Private Shared _Threshold As Numeric = New Numeric("Threshold", "Remote threshold", 1, False, 1000, 0, 5000, 0)
	Public Shared Property Threshold() As Numeric
		Get
			Return _Threshold
		End Get
		Set(value As Numeric)
			_Threshold = value
		End Set
	End Property
	Private Shared _PrinterName As PrinterList = New PrinterList("PrinterName", "Printer name", 1, True, "")
	Public Shared Property PrinterName() As PrinterList
		Get
			Return _PrinterName
		End Get
		Set(value As PrinterList)
			_PrinterName = value
		End Set
	End Property
	Private Shared _TicketPrinterPort As ComPortList = New ComPortList("TicketPrinterPort", "Printer com port", 1, True, 0)
	Public Shared Property TicketPrinterPort() As ComPortList
		Get
			Return _TicketPrinterPort
		End Get
		Set(value As ComPortList)
			_TicketPrinterPort = value
		End Set
	End Property
	Private Shared _RFIDPort As ComPortList = New ComPortList("RFIDPort", "RFID com port", 1, True, 0)
	Public Shared Property RFIDPort() As ComPortList
		Get
			Return _RFIDPort
		End Get
		Set(value As ComPortList)
			_RFIDPort = value
		End Set
	End Property
	Private Shared _RFIDBaud As Numeric = New Numeric("RFIDBaud", "RFID baud rate", 1, True, 9600, 0, 1152000, 100)
	Public Shared Property RFIDBaud() As Numeric
		Get
			Return _RFIDBaud
		End Get
		Set(value As Numeric)
			_RFIDBaud = value
		End Set
	End Property
	Public Enum opts_RFIDParity
		none = 0
		odd = 1
		even = 2
		mark = 3
		space = 4
	End Enum
	Private Shared _RFIDParity_none As PicklistOption = New PicklistOption("none", "None", 0)
	Public Shared Property RFIDParity_none() As PicklistOption
		Get
			Return _RFIDParity_none
		End Get
		Set(value As PicklistOption)
			_RFIDParity_none = value
		End Set
	End Property
	Private Shared _RFIDParity_odd As PicklistOption = New PicklistOption("odd", "Odd", 1)
	Public Shared Property RFIDParity_odd() As PicklistOption
		Get
			Return _RFIDParity_odd
		End Get
		Set(value As PicklistOption)
			_RFIDParity_odd = value
		End Set
	End Property
	Private Shared _RFIDParity_even As PicklistOption = New PicklistOption("even", "Even", 2)
	Public Shared Property RFIDParity_even() As PicklistOption
		Get
			Return _RFIDParity_even
		End Get
		Set(value As PicklistOption)
			_RFIDParity_even = value
		End Set
	End Property
	Private Shared _RFIDParity_mark As PicklistOption = New PicklistOption("mark", "Mark", 3)
	Public Shared Property RFIDParity_mark() As PicklistOption
		Get
			Return _RFIDParity_mark
		End Get
		Set(value As PicklistOption)
			_RFIDParity_mark = value
		End Set
	End Property
	Private Shared _RFIDParity_space As PicklistOption = New PicklistOption("space", "Space", 4)
	Public Shared Property RFIDParity_space() As PicklistOption
		Get
			Return _RFIDParity_space
		End Get
		Set(value As PicklistOption)
			_RFIDParity_space = value
		End Set
	End Property
	Private Shared _RFIDParity As Picklist = New Picklist("RFIDParity", "RFID parity", 1, True, 0, {RFIDParity_none, RFIDParity_odd, RFIDParity_even, RFIDParity_mark, RFIDParity_space})
	Public Shared Property RFIDParity() As Picklist
		Get
			Return _RFIDParity
		End Get
		Set(value As Picklist)
			_RFIDParity = value
		End Set
	End Property
	Private Shared _RFIDDataBits As Numeric = New Numeric("RFIDDataBits", "RFID data bits", 1, True, 8, 0, 8, 7)
	Public Shared Property RFIDDataBits() As Numeric
		Get
			Return _RFIDDataBits
		End Get
		Set(value As Numeric)
			_RFIDDataBits = value
		End Set
	End Property
	Private Shared _RFIDStopBits As Numeric = New Numeric("RFIDStopBits", "RFID stop bits", 1, True, 1, 0, 2, 0)
	Public Shared Property RFIDStopBits() As Numeric
		Get
			Return _RFIDStopBits
		End Get
		Set(value As Numeric)
			_RFIDStopBits = value
		End Set
	End Property
	Private Shared _RFIDStartChar As Numeric = New Numeric("RFIDStartChar", "RFID start char", 1, True, 0, 0, 31, 0)
	Public Shared Property RFIDStartChar() As Numeric
		Get
			Return _RFIDStartChar
		End Get
		Set(value As Numeric)
			_RFIDStartChar = value
		End Set
	End Property
	Private Shared _RFIDEndChar As Numeric = New Numeric("RFIDEndChar", "RFID end char", 1, True, 0, 0, 31, 0)
	Public Shared Property RFIDEndChar() As Numeric
		Get
			Return _RFIDEndChar
		End Get
		Set(value As Numeric)
			_RFIDEndChar = value
		End Set
	End Property
	Private Shared _RFIDIgnoreEnd As Numeric = New Numeric("RFIDIgnoreEnd", "RFID ignore end", 1, True, 0, 0, 255, 0)
	Public Shared Property RFIDIgnoreEnd() As Numeric
		Get
			Return _RFIDIgnoreEnd
		End Get
		Set(value As Numeric)
			_RFIDIgnoreEnd = value
		End Set
	End Property
	Private Shared _RFIDSettings As Subgroup = New Subgroup("RFIDSettings", "RFID Settings", 1, {RFIDPort, RFIDBaud, RFIDParity, RFIDDataBits, RFIDStopBits, RFIDStartChar, RFIDEndChar, RFIDIgnoreEnd})
	Public Shared Property RFIDSettings() As Subgroup
		Get
			Return _RFIDSettings
		End Get
		Set(value As Subgroup)
			_RFIDSettings = value
		End Set
	End Property
	Public Enum opts_RemoteType
		rinstrumSysTec = 0
		rinstrumRinstrum = 1
		rgbSysTec = 2
		rgbTScale = 3
		rgbDiniArgeo = 4
	End Enum
	Private Shared _RemoteType_rinstrumSysTec As PicklistOption = New PicklistOption("rinstrumSysTec", "Rinstrum/SysTec", 0)
	Public Shared Property RemoteType_rinstrumSysTec() As PicklistOption
		Get
			Return _RemoteType_rinstrumSysTec
		End Get
		Set(value As PicklistOption)
			_RemoteType_rinstrumSysTec = value
		End Set
	End Property
	Private Shared _RemoteType_rinstrumRinstrum As PicklistOption = New PicklistOption("rinstrumRinstrum", "Rinstrum/Rinstrum", 1)
	Public Shared Property RemoteType_rinstrumRinstrum() As PicklistOption
		Get
			Return _RemoteType_rinstrumRinstrum
		End Get
		Set(value As PicklistOption)
			_RemoteType_rinstrumRinstrum = value
		End Set
	End Property
	Private Shared _RemoteType_rgbSysTec As PicklistOption = New PicklistOption("rgbSysTec", "RGB/SysTec", 2)
	Public Shared Property RemoteType_rgbSysTec() As PicklistOption
		Get
			Return _RemoteType_rgbSysTec
		End Get
		Set(value As PicklistOption)
			_RemoteType_rgbSysTec = value
		End Set
	End Property
	Private Shared _RemoteType_rgbTScale As PicklistOption = New PicklistOption("rgbTScale", "RGB/TScale", 3)
	Public Shared Property RemoteType_rgbTScale() As PicklistOption
		Get
			Return _RemoteType_rgbTScale
		End Get
		Set(value As PicklistOption)
			_RemoteType_rgbTScale = value
		End Set
	End Property
	Private Shared _RemoteType_rgbDiniArgeo As PicklistOption = New PicklistOption("rgbDiniArgeo", "RGB/DiniArgeo", 4)
	Public Shared Property RemoteType_rgbDiniArgeo() As PicklistOption
		Get
			Return _RemoteType_rgbDiniArgeo
		End Get
		Set(value As PicklistOption)
			_RemoteType_rgbDiniArgeo = value
		End Set
	End Property
	Private Shared _RemoteType As Picklist = New Picklist("RemoteType", "Remote type", 1, True, 0, {RemoteType_rinstrumSysTec, RemoteType_rinstrumRinstrum, RemoteType_rgbSysTec, RemoteType_rgbTScale, RemoteType_rgbDiniArgeo})
	Public Shared Property RemoteType() As Picklist
		Get
			Return _RemoteType
		End Get
		Set(value As Picklist)
			_RemoteType = value
		End Set
	End Property
	Private Shared _RemotePort As ComPortList = New ComPortList("RemotePort", "Remote com port", 1, True, 0)
	Public Shared Property RemotePort() As ComPortList
		Get
			Return _RemotePort
		End Get
		Set(value As ComPortList)
			_RemotePort = value
		End Set
	End Property
	Private Shared _RemoteBaud As Numeric = New Numeric("RemoteBaud", "Remote baud rate", 1, True, 9600, 0, 1152000, 100)
	Public Shared Property RemoteBaud() As Numeric
		Get
			Return _RemoteBaud
		End Get
		Set(value As Numeric)
			_RemoteBaud = value
		End Set
	End Property
	Private Shared _RemoteSettings As Subgroup = New Subgroup("RemoteSettings", "Remote Settings", 1, {RemoteType, RemotePort, RemoteBaud})
	Public Shared Property RemoteSettings() As Subgroup
		Get
			Return _RemoteSettings
		End Get
		Set(value As Subgroup)
			_RemoteSettings = value
		End Set
	End Property
	Private Shared _devices As Group = New Group("devices", "Devices", 1, {WindowsPrinter, TicketCopies, OutputType, Threshold, PrinterName, TicketPrinterPort, RFIDSettings, RemoteSettings})
	Public Shared Property devices() As Group
		Get
			Return _devices
		End Get
		Set(value As Group)
			_devices = value
		End Set
	End Property
	Public Enum opts_Weighbridge
		inBridge = 0
		outBridge = 1
	End Enum
	Private Shared _Weighbridge_inBridge As PicklistOption = New PicklistOption("inBridge", "In bridge", 0)
	Public Shared Property Weighbridge_inBridge() As PicklistOption
		Get
			Return _Weighbridge_inBridge
		End Get
		Set(value As PicklistOption)
			_Weighbridge_inBridge = value
		End Set
	End Property
	Private Shared _Weighbridge_outBridge As PicklistOption = New PicklistOption("outBridge", "Out bridge", 1)
	Public Shared Property Weighbridge_outBridge() As PicklistOption
		Get
			Return _Weighbridge_outBridge
		End Get
		Set(value As PicklistOption)
			_Weighbridge_outBridge = value
		End Set
	End Property
	Private Shared _Weighbridge As Picklist = New Picklist("Weighbridge", "Weighbridge ID", 1, True, 0, {Weighbridge_inBridge, Weighbridge_outBridge})
	Public Shared Property Weighbridge() As Picklist
		Get
			Return _Weighbridge
		End Get
		Set(value As Picklist)
			_Weighbridge = value
		End Set
	End Property
	Public Enum opts_SiteID
		none = 0
	End Enum
	Private Shared _SiteID_none As PicklistOption = New PicklistOption("none", "None specified", 0)
	Public Shared Property SiteID_none() As PicklistOption
		Get
			Return _SiteID_none
		End Get
		Set(value As PicklistOption)
			_SiteID_none = value
		End Set
	End Property
	Private Shared _SiteID As Picklist = New Picklist("SiteID", "Site ID", 1, True, 0, {SiteID_none})
	Public Shared Property SiteID() As Picklist
		Get
			Return _SiteID
		End Get
		Set(value As Picklist)
			_SiteID = value
		End Set
	End Property
	Public Enum opts_DefaultCompany
		none = 0
	End Enum
	Private Shared _DefaultCompany_none As PicklistOption = New PicklistOption("none", "None specified", 0)
	Public Shared Property DefaultCompany_none() As PicklistOption
		Get
			Return _DefaultCompany_none
		End Get
		Set(value As PicklistOption)
			_DefaultCompany_none = value
		End Set
	End Property
	Private Shared _DefaultCompany As Picklist = New Picklist("DefaultCompany", "Default company", 1, True, 0, {DefaultCompany_none})
	Public Shared Property DefaultCompany() As Picklist
		Get
			Return _DefaultCompany
		End Get
		Set(value As Picklist)
			_DefaultCompany = value
		End Set
	End Property
	Private Shared _site As Group = New Group("site", "Site", 1, {Weighbridge, SiteID, DefaultCompany})
	Public Shared Property site() As Group
		Get
			Return _site
		End Get
		Set(value As Group)
			_site = value
		End Set
	End Property
	Private Shared _Units As Text = New Text("Units", "Units", 1, False, "kg", 3)
	Public Shared Property Units() As Text
		Get
			Return _Units
		End Get
		Set(value As Text)
			_Units = value
		End Set
	End Property
	Private Shared _FullScreen As Binary = New Binary("FullScreen", "Full Screen", 1, True, True)
	Public Shared Property FullScreen() As Binary
		Get
			Return _FullScreen
		End Get
		Set(value As Binary)
			_FullScreen = value
		End Set
	End Property
	Private Shared _TicketDesc As Text = New Text("TicketDesc", "Ticket Description", 1, False, "Ticket Printout", 50)
	Public Shared Property TicketDesc() As Text
		Get
			Return _TicketDesc
		End Get
		Set(value As Text)
			_TicketDesc = value
		End Set
	End Property
	Private Shared _CoAddr As Text = New Text("CoAddr", "Company Address", 1, False, "Units 4 &amp;amp; 5, Kimpton Enterprise Park, Herts, SG4 8HP", 255)
	Public Shared Property CoAddr() As Text
		Get
			Return _CoAddr
		End Get
		Set(value As Text)
			_CoAddr = value
		End Set
	End Property
	Private Shared _CoTelFax As Text = New Text("CoTelFax", "Company Telephone/Fax", 1, False, "Tel. +44 1438 000000 Fax. +44 1438 000000", 255)
	Public Shared Property CoTelFax() As Text
		Get
			Return _CoTelFax
		End Get
		Set(value As Text)
			_CoTelFax = value
		End Set
	End Property
	Private Shared _CoWeb As Text = New Text("CoWeb", "Company Website", 1, False, "http://www.archwood.co.uk", 255)
	Public Shared Property CoWeb() As Text
		Get
			Return _CoWeb
		End Get
		Set(value As Text)
			_CoWeb = value
		End Set
	End Property
	Private Shared _ActivationKey As Text = New Text("ActivationKey", "Activation key", 1, True, "F4-49-C5-E3", 16)
	Public Shared Property ActivationKey() As Text
		Get
			Return _ActivationKey
		End Get
		Set(value As Text)
			_ActivationKey = value
		End Set
	End Property
	Private Shared _AutoWindowsLogon As Binary = New Binary("AutoWindowsLogon", "Auto Windows logon?", 1, True, True)
	Public Shared Property AutoWindowsLogon() As Binary
		Get
			Return _AutoWindowsLogon
		End Get
		Set(value As Binary)
			_AutoWindowsLogon = value
		End Set
	End Property
	Private Shared _misc As Group = New Group("misc", "Misc", 100, {Units, FullScreen, TicketDesc, CoAddr, CoTelFax, CoWeb, ActivationKey, AutoWindowsLogon})
	Public Shared Property misc() As Group
		Get
			Return _misc
		End Get
		Set(value As Group)
			_misc = value
		End Set
	End Property
	Private Shared _groupsList As Group() = {general, weighbridges, devices, site, misc}
	Public Shared Property groupsList() As Group()
		Get
			Return _groupsList
		End Get
		Set(value As Group())
			_groupsList = value
		End Set
	End Property
	Public Shared Function getGroupByName(groupName As String)
		For Each group In groupsList
			If group.name = groupName Then
				Return group
			End If
		Next
		Return Nothing
	End Function
End Class
