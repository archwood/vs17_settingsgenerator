﻿Public Class clsAppFunctions

#Region "Enumerations, constants, etc."

    Public Enum ErrMsgState
        Critical = 16
        Question = 32
        Exclamation = 48
        Information = 64
    End Enum

#End Region



#Region "Error handling"

    ''' <summary>

    ''' Show message for unhandled exceptions

    ''' </summary>

    ''' <param name="objSender"></param>

    ''' <param name="strMethod"></param>

    ''' <param name="exSource"></param>

    ''' <param name="enErrMsgState"></param>

    ''' <remarks></remarks>

    Public Shared Sub ErrMsg(ByRef objSender As Object, ByVal strMethod As String, ByVal exSource As Exception, Optional enErrMsgState As ErrMsgState = ErrMsgState.Critical)

        Try
            Dim strSenderName As String
            Dim mbs As MsgBoxStyle
            Dim strMessage As String
            If objSender Is Nothing Then
                strSenderName = "clsAppInfo"
            Else
                strSenderName = objSender.Name
            End If
            mbs = enErrMsgState + MsgBoxStyle.OkOnly
            Dim st As New StackTrace(True)
            st = New StackTrace(exSource, True)

            strMessage = "RUN TIME ERROR" & vbCrLf & vbCrLf & "Object: " & strSenderName & vbCrLf &
                "Method: " & strMethod & vbCrLf & vbCrLf &
                "Library: " & exSource.Source.ToString & vbCrLf &
                "Function: " & exSource.TargetSite.ToString & vbCrLf & vbCrLf &
                "Error: " & exSource.Message & vbCrLf & vbCrLf &
                "Line: " & st.GetFrame(0).GetFileLineNumber().ToString
            '& "Error: " & exSource.Message & vbCrLf & vbCrLf _
            '& "Details: " & exSource.InnerException.ToString
            If exSource.InnerException IsNot Nothing Then
                strMessage = strMessage & vbCrLf & vbCrLf & "Details: " & exSource.InnerException.ToString
            End If
            MsgBox(strMessage, mbs)
        Catch ex As Exception
            MsgBox("ErrMsg() error: " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



#End Region
    ''' <summary>
    ''' Enables/disables buttons on specified form depending on access level of current user
    ''' </summary>
    ''' <param name="frm"></param>
    ''' <remarks>Calls recursive method EnableControlButtons for child controls</remarks>
    Public Shared Sub EnableFormButtons(ByRef frm As Form, ByVal accessLevel As Int32)     'COME BACK TO AND FIX!!!!!
        Try
            For Each ctl As Control In frm.Controls
                If ctl.Controls.Count <> 0 Then
                    EnableControlButtons(ctl, accessLevel)
                Else
                    If accessLevel >= ctl.Tag Then
                        ctl.Enabled = True
                    Else
                        ctl.Enabled = False
                    End If
                End If

            Next
        Catch ex As Exception
            ErrMsg(frm, "SetFormButtonAccess", ex)
        End Try
    End Sub

    ''' <summary>
    ''' Recursive method to enable/disable child controls dependent on access level
    ''' </summary>
    ''' <param name="ctlParent"></param>
    ''' <param name="intAccessLevel"></param>
    ''' <remarks>Requires tag on buttons to selectively disable</remarks>
    Private Shared Sub EnableControlButtons(ByRef ctlParent As Control, ByVal accessLevel As Int32)
        Try
            For Each child As Control In ctlParent.Controls
                If child.Controls.Count <> 0 Then
                    EnableControlButtons(child, accessLevel)
                Else
                    If accessLevel >= child.Tag Then
                        child.Enabled = True
                    Else
                        child.Enabled = False
                    End If
                End If

            Next
        Catch ex As Exception
            ErrMsg(Nothing, "EnableControlButtons", ex)
        End Try
    End Sub


End Class
