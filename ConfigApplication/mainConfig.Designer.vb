﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class mainConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainConfig))
        Me.btn_generate = New System.Windows.Forms.Button()
        Me.btn_settingDelete = New System.Windows.Forms.Button()
        Me.tb_ctrl = New System.Windows.Forms.TabControl()
        Me.of_dialog = New System.Windows.Forms.OpenFileDialog()
        Me.gb_settingProperties = New System.Windows.Forms.GroupBox()
        Me.chk_isFilePath = New System.Windows.Forms.CheckBox()
        Me.btn_editOptions = New System.Windows.Forms.Button()
        Me.cb_defaultValue = New System.Windows.Forms.ComboBox()
        Me.lbl_maxLength = New System.Windows.Forms.Label()
        Me.num_maxLength = New System.Windows.Forms.NumericUpDown()
        Me.lbl_decimalPlaces = New System.Windows.Forms.Label()
        Me.lbl_maxValue = New System.Windows.Forms.Label()
        Me.lbl_minValue = New System.Windows.Forms.Label()
        Me.num_decimalPlaces = New System.Windows.Forms.NumericUpDown()
        Me.num_maxValue = New System.Windows.Forms.NumericUpDown()
        Me.num_minValue = New System.Windows.Forms.NumericUpDown()
        Me.chk_defaultValue = New System.Windows.Forms.CheckBox()
        Me.txt_defaultValue = New System.Windows.Forms.TextBox()
        Me.num_defaultValue = New System.Windows.Forms.NumericUpDown()
        Me.cb_settingType = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_settingSave = New System.Windows.Forms.Button()
        Me.txt_settingLabel = New System.Windows.Forms.TextBox()
        Me.cb_settingViewIfNoAccess = New System.Windows.Forms.CheckBox()
        Me.num_settingAccessLevel = New System.Windows.Forms.NumericUpDown()
        Me.txt_settingName = New System.Windows.Forms.TextBox()
        Me.gb_groupProperties = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_groupDelete = New System.Windows.Forms.Button()
        Me.btn_groupSave = New System.Windows.Forms.Button()
        Me.num_groupAccessLevel = New System.Windows.Forms.NumericUpDown()
        Me.txt_groupLabel = New System.Windows.Forms.TextBox()
        Me.txt_groupName = New System.Windows.Forms.TextBox()
        Me.btn_newSetting = New System.Windows.Forms.Button()
        Me.btn_newGroup = New System.Windows.Forms.Button()
        Me.fb_dialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.btn_newSubgroup = New System.Windows.Forms.Button()
        Me.gb_subgroupProperties = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btn_subgroupDelete = New System.Windows.Forms.Button()
        Me.btn_subgroupSave = New System.Windows.Forms.Button()
        Me.num_subgroupAccessLevel = New System.Windows.Forms.NumericUpDown()
        Me.txt_subgroupLabel = New System.Windows.Forms.TextBox()
        Me.txt_subgroupName = New System.Windows.Forms.TextBox()
        Me.gb_settingProperties.SuspendLayout()
        CType(Me.num_maxLength, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_decimalPlaces, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_maxValue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_minValue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_defaultValue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_settingAccessLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gb_groupProperties.SuspendLayout()
        CType(Me.num_groupAccessLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gb_subgroupProperties.SuspendLayout()
        CType(Me.num_subgroupAccessLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_generate
        '
        Me.btn_generate.Location = New System.Drawing.Point(518, 260)
        Me.btn_generate.Name = "btn_generate"
        Me.btn_generate.Size = New System.Drawing.Size(94, 83)
        Me.btn_generate.TabIndex = 0
        Me.btn_generate.Text = "Save XML " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Generate Class"
        Me.btn_generate.UseVisualStyleBackColor = True
        '
        'btn_settingDelete
        '
        Me.btn_settingDelete.Location = New System.Drawing.Point(161, 287)
        Me.btn_settingDelete.Name = "btn_settingDelete"
        Me.btn_settingDelete.Size = New System.Drawing.Size(97, 23)
        Me.btn_settingDelete.TabIndex = 10
        Me.btn_settingDelete.Text = "Delete Setting"
        Me.btn_settingDelete.UseVisualStyleBackColor = True
        '
        'tb_ctrl
        '
        Me.tb_ctrl.Location = New System.Drawing.Point(12, 12)
        Me.tb_ctrl.Multiline = True
        Me.tb_ctrl.Name = "tb_ctrl"
        Me.tb_ctrl.SelectedIndex = 0
        Me.tb_ctrl.Size = New System.Drawing.Size(500, 331)
        Me.tb_ctrl.TabIndex = 5
        '
        'of_dialog
        '
        Me.of_dialog.Filter = "XML files (*.xml)|*.xml"
        '
        'gb_settingProperties
        '
        Me.gb_settingProperties.Controls.Add(Me.chk_isFilePath)
        Me.gb_settingProperties.Controls.Add(Me.btn_editOptions)
        Me.gb_settingProperties.Controls.Add(Me.cb_defaultValue)
        Me.gb_settingProperties.Controls.Add(Me.lbl_maxLength)
        Me.gb_settingProperties.Controls.Add(Me.num_maxLength)
        Me.gb_settingProperties.Controls.Add(Me.lbl_decimalPlaces)
        Me.gb_settingProperties.Controls.Add(Me.lbl_maxValue)
        Me.gb_settingProperties.Controls.Add(Me.lbl_minValue)
        Me.gb_settingProperties.Controls.Add(Me.num_decimalPlaces)
        Me.gb_settingProperties.Controls.Add(Me.num_maxValue)
        Me.gb_settingProperties.Controls.Add(Me.num_minValue)
        Me.gb_settingProperties.Controls.Add(Me.chk_defaultValue)
        Me.gb_settingProperties.Controls.Add(Me.txt_defaultValue)
        Me.gb_settingProperties.Controls.Add(Me.num_defaultValue)
        Me.gb_settingProperties.Controls.Add(Me.cb_settingType)
        Me.gb_settingProperties.Controls.Add(Me.Label8)
        Me.gb_settingProperties.Controls.Add(Me.Label4)
        Me.gb_settingProperties.Controls.Add(Me.Label3)
        Me.gb_settingProperties.Controls.Add(Me.Label2)
        Me.gb_settingProperties.Controls.Add(Me.Label1)
        Me.gb_settingProperties.Controls.Add(Me.btn_settingSave)
        Me.gb_settingProperties.Controls.Add(Me.txt_settingLabel)
        Me.gb_settingProperties.Controls.Add(Me.cb_settingViewIfNoAccess)
        Me.gb_settingProperties.Controls.Add(Me.num_settingAccessLevel)
        Me.gb_settingProperties.Controls.Add(Me.btn_settingDelete)
        Me.gb_settingProperties.Controls.Add(Me.txt_settingName)
        Me.gb_settingProperties.Location = New System.Drawing.Point(628, 13)
        Me.gb_settingProperties.Margin = New System.Windows.Forms.Padding(2)
        Me.gb_settingProperties.Name = "gb_settingProperties"
        Me.gb_settingProperties.Padding = New System.Windows.Forms.Padding(2)
        Me.gb_settingProperties.Size = New System.Drawing.Size(271, 331)
        Me.gb_settingProperties.TabIndex = 10
        Me.gb_settingProperties.TabStop = False
        Me.gb_settingProperties.Text = "Setting Properties"
        '
        'chk_isFilePath
        '
        Me.chk_isFilePath.AutoSize = True
        Me.chk_isFilePath.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chk_isFilePath.Location = New System.Drawing.Point(10, 213)
        Me.chk_isFilePath.Name = "chk_isFilePath"
        Me.chk_isFilePath.Size = New System.Drawing.Size(84, 17)
        Me.chk_isFilePath.TabIndex = 27
        Me.chk_isFilePath.Text = "Is File Path?"
        Me.chk_isFilePath.UseVisualStyleBackColor = True
        '
        'btn_editOptions
        '
        Me.btn_editOptions.Location = New System.Drawing.Point(93, 211)
        Me.btn_editOptions.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_editOptions.Name = "btn_editOptions"
        Me.btn_editOptions.Size = New System.Drawing.Size(165, 22)
        Me.btn_editOptions.TabIndex = 6
        Me.btn_editOptions.Text = "Edit Options"
        Me.btn_editOptions.UseVisualStyleBackColor = True
        '
        'cb_defaultValue
        '
        Me.cb_defaultValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_defaultValue.FormattingEnabled = True
        Me.cb_defaultValue.Location = New System.Drawing.Point(93, 173)
        Me.cb_defaultValue.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_defaultValue.Name = "cb_defaultValue"
        Me.cb_defaultValue.Size = New System.Drawing.Size(166, 21)
        Me.cb_defaultValue.TabIndex = 5
        '
        'lbl_maxLength
        '
        Me.lbl_maxLength.AutoSize = True
        Me.lbl_maxLength.Location = New System.Drawing.Point(9, 254)
        Me.lbl_maxLength.Name = "lbl_maxLength"
        Me.lbl_maxLength.Size = New System.Drawing.Size(63, 13)
        Me.lbl_maxLength.TabIndex = 26
        Me.lbl_maxLength.Text = "Max Length"
        '
        'num_maxLength
        '
        Me.num_maxLength.Location = New System.Drawing.Point(93, 252)
        Me.num_maxLength.Margin = New System.Windows.Forms.Padding(2)
        Me.num_maxLength.Maximum = New Decimal(New Integer() {1000000000, 0, 0, 0})
        Me.num_maxLength.Name = "num_maxLength"
        Me.num_maxLength.Size = New System.Drawing.Size(120, 20)
        Me.num_maxLength.TabIndex = 7
        '
        'lbl_decimalPlaces
        '
        Me.lbl_decimalPlaces.AutoSize = True
        Me.lbl_decimalPlaces.Location = New System.Drawing.Point(7, 214)
        Me.lbl_decimalPlaces.Name = "lbl_decimalPlaces"
        Me.lbl_decimalPlaces.Size = New System.Drawing.Size(80, 13)
        Me.lbl_decimalPlaces.TabIndex = 24
        Me.lbl_decimalPlaces.Text = "Decimal Places"
        '
        'lbl_maxValue
        '
        Me.lbl_maxValue.AutoSize = True
        Me.lbl_maxValue.Location = New System.Drawing.Point(181, 236)
        Me.lbl_maxValue.Name = "lbl_maxValue"
        Me.lbl_maxValue.Size = New System.Drawing.Size(57, 13)
        Me.lbl_maxValue.TabIndex = 23
        Me.lbl_maxValue.Text = "Max Value"
        '
        'lbl_minValue
        '
        Me.lbl_minValue.AutoSize = True
        Me.lbl_minValue.Location = New System.Drawing.Point(37, 236)
        Me.lbl_minValue.Name = "lbl_minValue"
        Me.lbl_minValue.Size = New System.Drawing.Size(54, 13)
        Me.lbl_minValue.TabIndex = 22
        Me.lbl_minValue.Text = "Min Value"
        '
        'num_decimalPlaces
        '
        Me.num_decimalPlaces.Location = New System.Drawing.Point(93, 212)
        Me.num_decimalPlaces.Name = "num_decimalPlaces"
        Me.num_decimalPlaces.Size = New System.Drawing.Size(120, 20)
        Me.num_decimalPlaces.TabIndex = 6
        '
        'num_maxValue
        '
        Me.num_maxValue.Location = New System.Drawing.Point(171, 252)
        Me.num_maxValue.Maximum = New Decimal(New Integer() {1569325056, 23283064, 0, 0})
        Me.num_maxValue.Name = "num_maxValue"
        Me.num_maxValue.Size = New System.Drawing.Size(71, 20)
        Me.num_maxValue.TabIndex = 8
        '
        'num_minValue
        '
        Me.num_minValue.Location = New System.Drawing.Point(28, 252)
        Me.num_minValue.Maximum = New Decimal(New Integer() {1569325056, 23283064, 0, 0})
        Me.num_minValue.Name = "num_minValue"
        Me.num_minValue.Size = New System.Drawing.Size(71, 20)
        Me.num_minValue.TabIndex = 7
        '
        'chk_defaultValue
        '
        Me.chk_defaultValue.AutoSize = True
        Me.chk_defaultValue.Location = New System.Drawing.Point(93, 176)
        Me.chk_defaultValue.Name = "chk_defaultValue"
        Me.chk_defaultValue.Size = New System.Drawing.Size(15, 14)
        Me.chk_defaultValue.TabIndex = 5
        Me.chk_defaultValue.UseVisualStyleBackColor = True
        '
        'txt_defaultValue
        '
        Me.txt_defaultValue.Location = New System.Drawing.Point(93, 173)
        Me.txt_defaultValue.Name = "txt_defaultValue"
        Me.txt_defaultValue.Size = New System.Drawing.Size(165, 20)
        Me.txt_defaultValue.TabIndex = 5
        '
        'num_defaultValue
        '
        Me.num_defaultValue.Location = New System.Drawing.Point(93, 173)
        Me.num_defaultValue.Name = "num_defaultValue"
        Me.num_defaultValue.Size = New System.Drawing.Size(120, 20)
        Me.num_defaultValue.TabIndex = 5
        '
        'cb_settingType
        '
        Me.cb_settingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_settingType.FormattingEnabled = True
        Me.cb_settingType.Items.AddRange(New Object() {"Text", "Numeric", "Binary", "Picklist", "ComPortList", "PrinterList", "PathLocation"})
        Me.cb_settingType.Location = New System.Drawing.Point(45, 99)
        Me.cb_settingType.Name = "cb_settingType"
        Me.cb_settingType.Size = New System.Drawing.Size(214, 21)
        Me.cb_settingType.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Type"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 177)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Default Value"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 135)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Access Level"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 66)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Label"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Name"
        '
        'btn_settingSave
        '
        Me.btn_settingSave.Location = New System.Drawing.Point(11, 287)
        Me.btn_settingSave.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_settingSave.Name = "btn_settingSave"
        Me.btn_settingSave.Size = New System.Drawing.Size(97, 23)
        Me.btn_settingSave.TabIndex = 9
        Me.btn_settingSave.Text = "Save Setting"
        Me.btn_settingSave.UseVisualStyleBackColor = True
        '
        'txt_settingLabel
        '
        Me.txt_settingLabel.Location = New System.Drawing.Point(46, 63)
        Me.txt_settingLabel.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_settingLabel.Name = "txt_settingLabel"
        Me.txt_settingLabel.Size = New System.Drawing.Size(213, 20)
        Me.txt_settingLabel.TabIndex = 1
        '
        'cb_settingViewIfNoAccess
        '
        Me.cb_settingViewIfNoAccess.AutoSize = True
        Me.cb_settingViewIfNoAccess.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cb_settingViewIfNoAccess.Location = New System.Drawing.Point(144, 136)
        Me.cb_settingViewIfNoAccess.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_settingViewIfNoAccess.Name = "cb_settingViewIfNoAccess"
        Me.cb_settingViewIfNoAccess.Size = New System.Drawing.Size(115, 17)
        Me.cb_settingViewIfNoAccess.TabIndex = 4
        Me.cb_settingViewIfNoAccess.Text = "View if no access?"
        Me.cb_settingViewIfNoAccess.UseVisualStyleBackColor = True
        '
        'num_settingAccessLevel
        '
        Me.num_settingAccessLevel.Location = New System.Drawing.Point(93, 133)
        Me.num_settingAccessLevel.Margin = New System.Windows.Forms.Padding(2)
        Me.num_settingAccessLevel.Name = "num_settingAccessLevel"
        Me.num_settingAccessLevel.Size = New System.Drawing.Size(38, 20)
        Me.num_settingAccessLevel.TabIndex = 3
        '
        'txt_settingName
        '
        Me.txt_settingName.Location = New System.Drawing.Point(46, 28)
        Me.txt_settingName.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_settingName.Name = "txt_settingName"
        Me.txt_settingName.Size = New System.Drawing.Size(213, 20)
        Me.txt_settingName.TabIndex = 0
        '
        'gb_groupProperties
        '
        Me.gb_groupProperties.Controls.Add(Me.Label7)
        Me.gb_groupProperties.Controls.Add(Me.Label6)
        Me.gb_groupProperties.Controls.Add(Me.Label5)
        Me.gb_groupProperties.Controls.Add(Me.btn_groupDelete)
        Me.gb_groupProperties.Controls.Add(Me.btn_groupSave)
        Me.gb_groupProperties.Controls.Add(Me.num_groupAccessLevel)
        Me.gb_groupProperties.Controls.Add(Me.txt_groupLabel)
        Me.gb_groupProperties.Controls.Add(Me.txt_groupName)
        Me.gb_groupProperties.Location = New System.Drawing.Point(628, 12)
        Me.gb_groupProperties.Margin = New System.Windows.Forms.Padding(2)
        Me.gb_groupProperties.Name = "gb_groupProperties"
        Me.gb_groupProperties.Padding = New System.Windows.Forms.Padding(2)
        Me.gb_groupProperties.Size = New System.Drawing.Size(271, 162)
        Me.gb_groupProperties.TabIndex = 12
        Me.gb_groupProperties.TabStop = False
        Me.gb_groupProperties.Text = "Group Properties"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 106)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Access Level"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 70)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Label"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 33)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Name"
        '
        'btn_groupDelete
        '
        Me.btn_groupDelete.Location = New System.Drawing.Point(161, 129)
        Me.btn_groupDelete.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_groupDelete.Name = "btn_groupDelete"
        Me.btn_groupDelete.Size = New System.Drawing.Size(97, 23)
        Me.btn_groupDelete.TabIndex = 4
        Me.btn_groupDelete.Text = "Delete Group"
        Me.btn_groupDelete.UseVisualStyleBackColor = True
        '
        'btn_groupSave
        '
        Me.btn_groupSave.Location = New System.Drawing.Point(11, 129)
        Me.btn_groupSave.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_groupSave.Name = "btn_groupSave"
        Me.btn_groupSave.Size = New System.Drawing.Size(97, 23)
        Me.btn_groupSave.TabIndex = 3
        Me.btn_groupSave.Text = "Save Group"
        Me.btn_groupSave.UseVisualStyleBackColor = True
        '
        'num_groupAccessLevel
        '
        Me.num_groupAccessLevel.Location = New System.Drawing.Point(86, 105)
        Me.num_groupAccessLevel.Margin = New System.Windows.Forms.Padding(2)
        Me.num_groupAccessLevel.Name = "num_groupAccessLevel"
        Me.num_groupAccessLevel.Size = New System.Drawing.Size(38, 20)
        Me.num_groupAccessLevel.TabIndex = 2
        '
        'txt_groupLabel
        '
        Me.txt_groupLabel.Location = New System.Drawing.Point(46, 67)
        Me.txt_groupLabel.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_groupLabel.Name = "txt_groupLabel"
        Me.txt_groupLabel.Size = New System.Drawing.Size(213, 20)
        Me.txt_groupLabel.TabIndex = 1
        '
        'txt_groupName
        '
        Me.txt_groupName.Location = New System.Drawing.Point(46, 31)
        Me.txt_groupName.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_groupName.Name = "txt_groupName"
        Me.txt_groupName.Size = New System.Drawing.Size(213, 20)
        Me.txt_groupName.TabIndex = 0
        '
        'btn_newSetting
        '
        Me.btn_newSetting.Location = New System.Drawing.Point(517, 164)
        Me.btn_newSetting.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_newSetting.Name = "btn_newSetting"
        Me.btn_newSetting.Size = New System.Drawing.Size(94, 62)
        Me.btn_newSetting.TabIndex = 13
        Me.btn_newSetting.Text = "New Setting"
        Me.btn_newSetting.UseVisualStyleBackColor = True
        '
        'btn_newGroup
        '
        Me.btn_newGroup.Location = New System.Drawing.Point(517, 12)
        Me.btn_newGroup.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_newGroup.Name = "btn_newGroup"
        Me.btn_newGroup.Size = New System.Drawing.Size(94, 64)
        Me.btn_newGroup.TabIndex = 14
        Me.btn_newGroup.Text = "New Group"
        Me.btn_newGroup.UseVisualStyleBackColor = True
        '
        'fb_dialog
        '
        Me.fb_dialog.Description = "Select location for file output"
        Me.fb_dialog.RootFolder = System.Environment.SpecialFolder.MyComputer
        '
        'btn_newSubgroup
        '
        Me.btn_newSubgroup.Location = New System.Drawing.Point(517, 89)
        Me.btn_newSubgroup.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_newSubgroup.Name = "btn_newSubgroup"
        Me.btn_newSubgroup.Size = New System.Drawing.Size(94, 62)
        Me.btn_newSubgroup.TabIndex = 16
        Me.btn_newSubgroup.Text = "New Subgroup"
        Me.btn_newSubgroup.UseVisualStyleBackColor = True
        '
        'gb_subgroupProperties
        '
        Me.gb_subgroupProperties.Controls.Add(Me.Label9)
        Me.gb_subgroupProperties.Controls.Add(Me.Label10)
        Me.gb_subgroupProperties.Controls.Add(Me.Label11)
        Me.gb_subgroupProperties.Controls.Add(Me.btn_subgroupDelete)
        Me.gb_subgroupProperties.Controls.Add(Me.btn_subgroupSave)
        Me.gb_subgroupProperties.Controls.Add(Me.num_subgroupAccessLevel)
        Me.gb_subgroupProperties.Controls.Add(Me.txt_subgroupLabel)
        Me.gb_subgroupProperties.Controls.Add(Me.txt_subgroupName)
        Me.gb_subgroupProperties.Location = New System.Drawing.Point(628, 12)
        Me.gb_subgroupProperties.Margin = New System.Windows.Forms.Padding(2)
        Me.gb_subgroupProperties.Name = "gb_subgroupProperties"
        Me.gb_subgroupProperties.Padding = New System.Windows.Forms.Padding(2)
        Me.gb_subgroupProperties.Size = New System.Drawing.Size(271, 162)
        Me.gb_subgroupProperties.TabIndex = 14
        Me.gb_subgroupProperties.TabStop = False
        Me.gb_subgroupProperties.Text = "Subgroup Properties"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 106)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Access Level"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 70)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Label"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 33)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Name"
        '
        'btn_subgroupDelete
        '
        Me.btn_subgroupDelete.Location = New System.Drawing.Point(161, 129)
        Me.btn_subgroupDelete.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_subgroupDelete.Name = "btn_subgroupDelete"
        Me.btn_subgroupDelete.Size = New System.Drawing.Size(97, 23)
        Me.btn_subgroupDelete.TabIndex = 4
        Me.btn_subgroupDelete.Text = "Delete Subgroup"
        Me.btn_subgroupDelete.UseVisualStyleBackColor = True
        '
        'btn_subgroupSave
        '
        Me.btn_subgroupSave.Location = New System.Drawing.Point(11, 129)
        Me.btn_subgroupSave.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_subgroupSave.Name = "btn_subgroupSave"
        Me.btn_subgroupSave.Size = New System.Drawing.Size(97, 23)
        Me.btn_subgroupSave.TabIndex = 3
        Me.btn_subgroupSave.Text = "Save Subgroup"
        Me.btn_subgroupSave.UseVisualStyleBackColor = True
        '
        'num_subgroupAccessLevel
        '
        Me.num_subgroupAccessLevel.Location = New System.Drawing.Point(86, 105)
        Me.num_subgroupAccessLevel.Margin = New System.Windows.Forms.Padding(2)
        Me.num_subgroupAccessLevel.Name = "num_subgroupAccessLevel"
        Me.num_subgroupAccessLevel.Size = New System.Drawing.Size(38, 20)
        Me.num_subgroupAccessLevel.TabIndex = 2
        '
        'txt_subgroupLabel
        '
        Me.txt_subgroupLabel.Location = New System.Drawing.Point(46, 67)
        Me.txt_subgroupLabel.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_subgroupLabel.Name = "txt_subgroupLabel"
        Me.txt_subgroupLabel.Size = New System.Drawing.Size(213, 20)
        Me.txt_subgroupLabel.TabIndex = 1
        '
        'txt_subgroupName
        '
        Me.txt_subgroupName.Location = New System.Drawing.Point(46, 31)
        Me.txt_subgroupName.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_subgroupName.Name = "txt_subgroupName"
        Me.txt_subgroupName.Size = New System.Drawing.Size(213, 20)
        Me.txt_subgroupName.TabIndex = 0
        '
        'mainConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1137, 445)
        Me.Controls.Add(Me.gb_subgroupProperties)
        Me.Controls.Add(Me.btn_newSubgroup)
        Me.Controls.Add(Me.btn_newGroup)
        Me.Controls.Add(Me.btn_newSetting)
        Me.Controls.Add(Me.gb_settingProperties)
        Me.Controls.Add(Me.tb_ctrl)
        Me.Controls.Add(Me.btn_generate)
        Me.Controls.Add(Me.gb_groupProperties)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "mainConfig"
        Me.Text = "Config Utility"
        Me.gb_settingProperties.ResumeLayout(False)
        Me.gb_settingProperties.PerformLayout()
        CType(Me.num_maxLength, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_decimalPlaces, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_maxValue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_minValue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_defaultValue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_settingAccessLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gb_groupProperties.ResumeLayout(False)
        Me.gb_groupProperties.PerformLayout()
        CType(Me.num_groupAccessLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gb_subgroupProperties.ResumeLayout(False)
        Me.gb_subgroupProperties.PerformLayout()
        CType(Me.num_subgroupAccessLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btn_generate As Button
    Friend WithEvents btn_settingDelete As Button
    Friend WithEvents tb_ctrl As TabControl
    Friend WithEvents of_dialog As OpenFileDialog
    Friend WithEvents gb_settingProperties As GroupBox
    Friend WithEvents txt_settingLabel As TextBox
    Friend WithEvents cb_settingViewIfNoAccess As CheckBox
    Friend WithEvents num_settingAccessLevel As NumericUpDown
    Friend WithEvents txt_settingName As TextBox
    Friend WithEvents btn_settingSave As Button
    Friend WithEvents gb_groupProperties As GroupBox
    Friend WithEvents txt_groupLabel As TextBox
    Friend WithEvents txt_groupName As TextBox
    Friend WithEvents btn_groupDelete As Button
    Friend WithEvents btn_groupSave As Button
    Friend WithEvents num_groupAccessLevel As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btn_newSetting As Button
    Friend WithEvents btn_newGroup As Button
    Friend WithEvents cb_settingType As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents lbl_decimalPlaces As Label
    Friend WithEvents lbl_maxValue As Label
    Friend WithEvents lbl_minValue As Label
    Friend WithEvents num_decimalPlaces As NumericUpDown
    Friend WithEvents num_maxValue As NumericUpDown
    Friend WithEvents num_minValue As NumericUpDown
    Friend WithEvents chk_defaultValue As CheckBox
    Friend WithEvents txt_defaultValue As TextBox
    Friend WithEvents num_defaultValue As NumericUpDown
    Friend WithEvents lbl_maxLength As Label
    Friend WithEvents num_maxLength As NumericUpDown
    Friend WithEvents btn_editOptions As Button
    Friend WithEvents cb_defaultValue As ComboBox
    Friend WithEvents fb_dialog As FolderBrowserDialog
    Friend WithEvents chk_isFilePath As CheckBox
    Friend WithEvents btn_newSubgroup As Button
    Friend WithEvents gb_subgroupProperties As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btn_subgroupDelete As Button
    Friend WithEvents btn_subgroupSave As Button
    Friend WithEvents num_subgroupAccessLevel As NumericUpDown
    Friend WithEvents txt_subgroupLabel As TextBox
    Friend WithEvents txt_subgroupName As TextBox
End Class
