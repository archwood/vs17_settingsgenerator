﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ConfigApplication - For new class")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Archwood")>
<Assembly: AssemblyProduct("ConfigApplication - For new class")>
<Assembly: AssemblyCopyright("")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9aafcbe1-5415-4990-9635-dcff369700d4")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.3.*")>
<Assembly: AssemblyFileVersion("1.0.3")>
