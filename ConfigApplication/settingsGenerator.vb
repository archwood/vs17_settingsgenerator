﻿Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization

Public Class settingsGenerator

    Public Shared writer As StreamWriter

    Public Shared Sub createChildProperty(child As Object, Optional sharedBool As Boolean = True, Optional objectNameOverride As String = Nothing)

        If child.GetType().Name = "Subgroup" Then

            Dim subgroup As Subgroup = child

            For Each subChild In subgroup.children
                createChildProperty(subChild)
            Next


        End If

        If child.GetType().Name = "Picklist" Then ' Need to pull out the picklist to generate all the options that came before it

            Dim picklist As Picklist = child

            createOptionsEnum(picklist.name, picklist.options)

            For Each opt In picklist.options
                createChildProperty(opt, objectNameOverride:=picklist.name & "_" & opt.name)
            Next

        End If

        Dim constructor As String = child.getConstructorString() ' Rather than createSetting/createGroup/createSubgroup, just add this function to return

        ' TODO - need to add the picklist option name override back in
        Dim name = child.name

        If objectNameOverride IsNot Nothing Then
            name = objectNameOverride
        End If

        ' Create property for child (setting/subgroup)
        createProperty(name, child.GetType().Name, constructor, sharedBool)


    End Sub
    Public Shared Sub generateClass(xmlFilePath As String, vbFilePath As String)

        Try

            ' Open Writer for clsSettings file '

            writer = New StreamWriter(vbFilePath)

            writer.WriteLine("Imports clsSettingsManager") ' Needs reference to DLL

            writer.WriteLine("Public Class clsSettings")   'starts class declaration

            ' Takes the XML file and loops through the Setting nodes to generate VB code to instantiate classes such that can be accessed in a program

            Try

                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Group()), New XmlRootAttribute("SettingsFile"))

                Dim reader As New StreamReader(xmlFilePath)

                Dim groups As Group() = serializer.Deserialize(reader) ' Deserialise the settings stored in the xml file and store in an array

                ' Cycle through each group and create properties for each one using the embedded function constructorString function

                For Each group As Group In groups

                    Dim children As New List(Of String)

                    For Each child In group.children

                        createChildProperty(child)

                    Next

                    createProperty(group.name, group.GetType().Name, group.getConstructorString(), True) ' Create the property for the Group itself

                Next

                Dim groupNames = groups.Select(Of String)(Function(g) g.name)

                Dim groupsString As String = Join(groupNames.ToArray, ", ")

                createProperty("groupsList", "Group()", "{" & groupsString & "}", True) 'collection of group properties


            Catch ex As Exception

                Throw ex

                createProperty("groupsList", "Group()", "{}", True) 'dummy list

            End Try

            writeClassMethods()

        Catch x As Exception

            clsAppFunctions.ErrMsg(New Object, "generateClass()", x)

        Finally

            closeWriter()   'If writing fails this makes sure the classes are written to prevent errors elsewhere in the solution

        End Try


    End Sub

#Region "Property Creators"
    Public Shared Sub createProperty(name As String, type As String, value As String, sharedBool As Boolean, Optional privateBool As Boolean = False)  'Writes the code for a property

        Dim propertyString As String = ""
        Dim sharedString As String = ""
        Dim propertyScopeString As String = "Public"

        If sharedBool Then
            sharedString = "Shared"
        End If

        If privateBool Then
            propertyScopeString = "Private"
        End If

        'Select case statement to get correct type string for the declaration
        propertyString += "Private " & sharedString & " _" & name & " as " & type & " = " & value & vbCrLf
        propertyString += propertyScopeString & " " & sharedString & " Property " & name & "() as " & type & vbCrLf
        propertyString += vbTab & "Get" & vbCrLf & vbTab & vbTab & "Return _" & name & vbCrLf
        propertyString += vbTab & "End Get" & vbCrLf
        propertyString += vbTab & "Set (value As " & type & ")" & vbCrLf & vbTab & vbTab & "_" & name & " = value" & vbCrLf
        propertyString += vbTab & "End Set" & vbCrLf & "End Property"

        writer.WriteLine(propertyString)
    End Sub

    'Null
    Public Shared Sub createProperty(name As String, type As String, sharedBool As Boolean)

        Dim propertyString As String = ""

        Dim sharedString As String = ""

        If sharedBool Then
            sharedString = "Shared"
        End If

        'Select case statement to get correct type string for the declaration
        propertyString += "Private " & sharedString & " _" & name & " as " & type & vbCrLf
        propertyString += "Public " & sharedString & " Property " & name & "() as " & type & vbCrLf
        propertyString += vbTab & "Get" & vbCrLf & vbTab & vbTab & "Return _" & name & vbCrLf
        propertyString += vbTab & "End Get" & vbCrLf
        propertyString += vbTab & "Set (value As " & type & ")" & vbCrLf & vbTab & vbTab & "_" & name & " = value" & vbCrLf
        propertyString += vbTab & "End Set" & vbCrLf & "End Property"

        writer.WriteLine(propertyString)
    End Sub

#End Region


    ' Takes the options nodelist and setting name and generates a public Enum in clsSettings with the structure
    ' Public Enum opts_SETTINGNAME
    '   optionName
    '   optionName
    ' End Enum
    Public Shared Sub createOptionsEnum(settingName As String, options As List(Of PicklistOption))

        Dim enumString As String = "Public Enum opts_" & settingName & vbCrLf ' Build up enumeration here

        For Each opt As PicklistOption In options

            enumString += opt.name

            ' If we have defined a vlaue for the PicklistOption then we need to add to the enum
            Dim xValue = opt.value.ToString()

            If xValue IsNot Nothing Then
                enumString += " = " & xValue
            End If

            enumString += vbCrLf
        Next

        enumString += "End Enum"

        writer.WriteLine(enumString) ' Actually write into the settingsFile
    End Sub



    Public Shared Sub writeClassMethods()
        writer.WriteLine("Public Shared Function getGroupByName(groupName As String)")
        writer.WriteLine(vbTab & "For Each group As Group In groupsList")
        writer.WriteLine(vbTab & vbTab & "If group.name = groupName Then")
        writer.WriteLine(vbTab & vbTab & vbTab & "Return group")
        writer.WriteLine(vbTab & vbTab & "End If")
        writer.WriteLine(vbTab & "Next")
        writer.WriteLine(vbTab & "Return Nothing")
        writer.WriteLine("End Function")
    End Sub



    'Used to finish the class's declaration and close the writer object
    Public Shared Sub closeWriter()

        writer.WriteLine("End Class")

        writer.Close()

    End Sub
End Class
