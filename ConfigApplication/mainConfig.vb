﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Xml.Serialization

Public Class mainConfig

    Private groups As New List(Of Group)

    Private xmlFilePath As String

    Private currentOptions As PicklistOption()

    Private openFilePath As String
    Public Function open(editBool As Boolean) As Boolean

        ' Have Root nodes be group.children
        ' Any children that are subgroups will be given child nodes of their children settings 
        ' Not caring about level beyond that currently
        ' Ask Jes about this 
        ' Subgroup settings will be exactly the same as group - just change the label of the groupbox 

        ' Start with all group boxes hidden
        gb_groupProperties.Visible = False
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False

        If editBool Then

            of_dialog.ShowDialog()

            openFilePath = of_dialog.FileName

            xmlFilePath = openFilePath

            Try

                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Group()), New XmlRootAttribute("SettingsFile"))

                Dim reader As New StreamReader(xmlFilePath)

                Dim xGroups As Group() = serializer.Deserialize(reader) ' Deserialise the settings stored in the xml file and store in an array

                reader.Close()

                groups.AddRange(xGroups) ' Add to global variable to be accesible across application

                groups.ForEach(Sub(g) loadGroupAsTabPage(g)) ' For each group, load as a tab page

            Catch ex As Exception

                MsgBox(ex.ToString)

                Return False ' Fail

            End Try

        Else

            Dim placeholderSetting As Text = New Text("placeholderSetting", "PLACEHOLDER - REPLACE", 1, True, "PLACEHOLDER VALUE", 255)

            Dim placeholderGroup As Group = New Group("general", "General", 1, {placeholderSetting})

            groups.Add(placeholderGroup)

            loadGroupAsTabPage(placeholderGroup)

        End If

        Me.Show()

        tb_ctrl_SelectedIndexChanged() ' Trigger group properties box

        Return True ' Success

    End Function

#Region "Form Events"

    Private Function getNodesFromChildren(children As List(Of Child)) As List(Of TreeNode) ' Returns list of TreeNodes recursively built from list of children (subgroups/settings)

        Dim nodes As New List(Of TreeNode) ' Create new list

        For Each child In children ' Cycle through all children

            Dim node As New TreeNode

            node.Text = child.name ' Set Text to name of child

            If child.GetType().Name = "Subgroup" Then ' Go level deeper and search through

                Dim subgroup As Subgroup = child

                Dim innerNodes As List(Of TreeNode) = getNodesFromChildren(subgroup.children)

                node.Nodes.AddRange(innerNodes.ToArray) ' Get nodes of subgroup children and add to nodes collection of subgroup node

            End If

            nodes.Add(node)

        Next

        Return nodes

    End Function

    Private Sub btn_newGroup_Click(sender As Object, e As EventArgs) Handles btn_newGroup.Click

        toggleMainControlState(False)

        txt_groupName.Focus()

        Dim newGroupTab As TabPage = New TabPage("<NEW GROUP>")

        tb_ctrl.TabPages.Add(newGroupTab)

        tb_ctrl.SelectedTab = newGroupTab

        Dim treeView As TreeView = New TreeView()

        treeView.Width = tb_ctrl.Width
        treeView.Height = tb_ctrl.Height

        newGroupTab.Controls.Add(treeView)

        AddHandler treeView.AfterSelect, AddressOf childSelection

        ' Hide setting properties and show group properties
        gb_groupProperties.Visible = True
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False

        clearGroupProperties()

    End Sub

    Private Sub btn_newSubgroup_Click(sender As Object, e As EventArgs) Handles btn_newSubgroup.Click

        ' TODO - Same as adding a new setting except we add to the treeview instead

        toggleMainControlState(False)

        ' Clear subgroup properties
        txt_subgroupName.Clear()
        txt_subgroupLabel.Clear()
        num_subgroupAccessLevel.Value = 1

        ' Add a new temp item to the selected list box/group
        Dim treeView As TreeView = getCurrentSettingsTreeView()

        Dim newNode = treeView.Nodes.Add("<NEW SUBGROUP>") ' Add Subgroup to treeView root

        treeView.SelectedNode = newNode

        ' Show subgroup properties
        gb_subgroupProperties.Visible = True
        gb_groupProperties.Visible = False
        gb_settingProperties.Visible = False

        txt_subgroupName.Focus()

    End Sub

    Private Sub btn_newSetting_Click(sender As Object, e As EventArgs) Handles btn_newSetting.Click

        toggleMainControlState(False)

        ' Need to reset the form
        cb_settingType.SelectedItem = "Text" ' Triggers selectedValueChanged on cb_settingType
        cb_settingType.Enabled = True
        txt_settingName.Clear()
        txt_settingLabel.Clear()
        num_settingAccessLevel.Value = 1
        cb_settingViewIfNoAccess.Checked = True

        ' Add a new temp item to the selected list box/group
        Dim treeView As TreeView = getCurrentSettingsTreeView()

        ' TODO - need to consider adding to subgroup here - but only allow one layer 

        Dim parentNodes = treeView.Nodes

        Dim selectedNode = treeView.SelectedNode

        If selectedNode IsNot Nothing Then
            If selectedNode.Nodes.Count > 0 Then ' If node has children then its a subgroup and we must add to subgroup
                parentNodes = selectedNode.Nodes
            ElseIf selectedNode.Parent IsNot Nothing Then
                parentNodes = selectedNode.Parent.Nodes
            End If
        End If

        Dim newNode = parentNodes.add("<NEW SETTING>")

        treeView.SelectedNode = newNode ' Set selection on new node which should trigger childSelection

        ' Switch to setting properties view
        gb_settingProperties.Visible = True
        gb_groupProperties.Visible = False
        gb_subgroupProperties.Visible = False

        txt_settingName.Focus() ' Focus on the name box

    End Sub

    Private Sub btn_generate_Click(sender As Object, e As EventArgs) Handles btn_generate.Click

        If Not String.IsNullOrEmpty(openFilePath) Then ' This opens the save directory at the point we opened an existing file if that was the case
            fb_dialog.SelectedPath = Path.GetDirectoryName(openFilePath)
        End If

        If fb_dialog.ShowDialog <> DialogResult.OK Then ' Check user actually selected folder location
            MsgBox("Save destination not selected, aborted file generation")
            Return
        End If

        ' Set file paths for both xml and vb class

        xmlFilePath = fb_dialog.SelectedPath & "/settings.xml"

        Dim vbFilePath = fb_dialog.SelectedPath & "/clsSettings.vb"


        Try ' Convert list of groups to XML file with root SettingsFile and then generate the clsSettings file from that using settingsGenerator

            Dim serializer As XmlSerializer = New XmlSerializer(GetType(Group()), New XmlRootAttribute("SettingsFile"))

            Dim writer As New StreamWriter(xmlFilePath)

            serializer.Serialize(writer, groups.ToArray)

            writer.Close()

            settingsGenerator.generateClass(xmlFilePath, vbFilePath)

            MsgBox("Class Generated")

        Catch ex As Exception

            clsAppFunctions.ErrMsg(sender, "btn_generate_click", ex)

        End Try

    End Sub

    Private Sub tb_ctrl_SelectedIndexChanged() Handles tb_ctrl.SelectedIndexChanged ' Triggered when user changes which Group is selected

        ' Hide setting properties and show group properties
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False
        gb_groupProperties.Visible = True

        If tb_ctrl.TabPages.Count > groups.Count Then ' This is for when a new unsaved Group has been added to the control
            clearGroupProperties()
            Return
        End If

        ' Get currently selected group as corresponds to the tab - NOTE, will need to be careful here when adding groups and tabs etc
        Dim selectedGroup As Group = getCurrentSelectedGroup()

        ' Populate group property controls
        txt_groupName.Text = selectedGroup.name
        txt_groupLabel.Text = selectedGroup.label
        num_groupAccessLevel.Value = selectedGroup.accessLevel

    End Sub

    Private Sub mainConfig_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        startup.Close()
    End Sub

#End Region

#Region "Form Functions"
    Private Sub clearGroupProperties()
        txt_groupName.Clear()
        txt_groupLabel.Clear()
        num_groupAccessLevel.Value = 1
    End Sub

    Private Sub clearSubgroupProperties()
        txt_subgroupName.Clear()
        txt_subgroupLabel.Clear()
        num_subgroupAccessLevel.Value = 1
    End Sub

    Private Sub hideAndClearValueControls()

        'Hide default values controls
        txt_defaultValue.Visible = False
        num_defaultValue.Visible = False
        chk_defaultValue.Visible = False
        cb_defaultValue.Visible = False

        ' Hide property controls
        num_decimalPlaces.Visible = False
        num_maxLength.Visible = False
        num_maxValue.Visible = False
        num_minValue.Visible = False
        btn_editOptions.Visible = False
        chk_isFilePath.Visible = False

        ' Reset values of controls
        txt_defaultValue.Clear()
        num_minValue.Value = 0
        num_maxValue.Value = 1000
        num_defaultValue.Minimum = num_minValue.Value
        num_defaultValue.Maximum = num_maxValue.Value
        num_defaultValue.Value = 0
        chk_defaultValue.Checked = False
        num_maxValue.Value = 0
        num_minValue.Value = 0
        num_maxLength.Value = 255
        num_decimalPlaces.Value = 0
        cb_defaultValue.Items.Clear()
        chk_isFilePath.Checked = False

        ' Hide Labels
        lbl_decimalPlaces.Visible = False
        lbl_maxLength.Visible = False
        lbl_maxValue.Visible = False
        lbl_minValue.Visible = False

    End Sub

    Private Function getCurrentSelectedGroup() As Group

        Try

            Dim selectedGroup As Group = groups(tb_ctrl.SelectedIndex)

            If selectedGroup.name <> tb_ctrl.SelectedTab.Text Then
                Throw New Exception("getCurrentSelectedGroup(): Group name does not match TabPage Text")
            End If

            Return selectedGroup

        Catch ex As Exception

            Throw ex

        End Try

    End Function

    Private Sub refreshCurrentList() ' This is used to refresh a specific list when performing a CRUD operation on children

        Dim treeView As TreeView = getCurrentSettingsTreeView()

        treeView.Nodes.Clear()

        Dim currentGroup As Group = getCurrentSelectedGroup()

        Dim children As List(Of Child) = currentGroup.children

        Dim nodes As List(Of TreeNode) = getNodesFromChildren(children)

        treeView.Nodes.AddRange(nodes.ToArray)

        treeView.ExpandAll()

    End Sub

    Private Sub loadGroupAsTabPage(group As Group)

        ' Create a TabPage for each group
        Dim tabPage As TabPage = New TabPage(group.name)

        tb_ctrl.TabPages.Add(tabPage)

        ' Changed listbox to treeview

        Dim childrenTree As New TreeView

        childrenTree.Width = tb_ctrl.Width

        childrenTree.Height = tb_ctrl.Height

        tabPage.Controls.Add(childrenTree) ' Add TreeView to tabpage

        Dim children = group.children

        Dim nodes As List(Of TreeNode) = getNodesFromChildren(children) ' Get list of TreeNodes as defined by group children

        childrenTree.Nodes.AddRange(nodes.ToArray) ' Had to add to array because it doesn't like List(of..)

        AddHandler childrenTree.AfterSelect, AddressOf childSelection ' Selection event for treeview nodes

    End Sub

    Private Function getCurrentSelectedSetting() ' Returns selected child  

        Dim treeView As TreeView = getCurrentSettingsTreeView()

        Dim selectedGroup As Group = getCurrentSelectedGroup()

        Dim selectedSetting = selectedGroup.getSettingByName(treeView.SelectedNode.Text)

        Return selectedSetting ' will return nothing if not found

    End Function

    Private Function getCurrentSelectedChild()

        Dim treeView As TreeView = getCurrentSettingsTreeView()

        Dim selectedGroup As Group = getCurrentSelectedGroup()

        Dim selectedChild = selectedGroup.getChildByName(treeView.SelectedNode.Text)

        Return selectedChild ' Will return nothing if not found

    End Function

    Private Function getCurrentSettingsTreeView() As TreeView

        Dim currentPage As TabPage = tb_ctrl.SelectedTab

        If currentPage.Controls(0).GetType().Name <> "TreeView" Then
            Throw New Exception("btn_newSetting_Click: Could not find TreeView on current TabPage")
        End If

        Dim treeView As TreeView = currentPage.Controls(0)

        Return treeView

    End Function

    Private Function countChildlessNodes(node As TreeNode)

        Dim count = 0

        If node.Nodes.Count = 0 Then
            count += 1
        Else
            For Each child As TreeNode In node.Nodes
                count += countChildlessNodes(child)
            Next
        End If

        Return count

    End Function

    ''' <summary>
    ''' Checks to see if number of settings in selected group matches number of elements in ListBox
    ''' </summary>
    ''' <returns>True if all settings have been </returns>
    Private Function areThereAnyUnsavedNewSettings() As Boolean

        Dim treeView As TreeView = getCurrentSettingsTreeView()

        Dim selectedGroup As Group = getCurrentSelectedGroup()

        Dim settingCount = 0 ' This will be number of childless nodes in treeview as all subgroups should have one child

        For Each node As TreeNode In treeView.Nodes
            settingCount += countChildlessNodes(node)
        Next

        Return settingCount <> selectedGroup.getSettings().Count ' If unsaved settings then settingCount will not equal number of settings in group

    End Function

    Private Function areThereAnyUnsavedNewSubgroups() As Boolean

        Dim treeView As TreeView = getCurrentSettingsTreeView()

        Dim selectedGroup As Group = getCurrentSelectedGroup()

        'Dim numberOfSubgroups = 0 ' IMPORTANT - THIS CODE ASSUMES SUBGROUPS ARE ONLY ALLOWED ON THE FIRST LAYER (FOR NOW)

        'For Each node As TreeNode In treeView.Nodes
        '    If node.Nodes.Count > 0 Then
        '        numberOfSubgroups += 1
        '    End If
        'Next

        Return treeView.Nodes.Count <> selectedGroup.children.Count ' If there is an unsaved subgroup this should show as an inequality between groups children and treeview nodes (assuming no unsaved settings and only first layer of subgroups)

    End Function

    Private Function areThereAnyUnsavedNewGroups() As Boolean

        Return tb_ctrl.TabPages.Count <> groups.Count ' tabpages should match groups 

    End Function

    Private Sub toggleMainControlState(enabled As Boolean)

        Dim mainControls As Control() = {tb_ctrl, btn_newGroup, btn_newSubgroup, btn_newSetting, btn_generate}

        For Each control In mainControls
            control.Enabled = enabled
        Next

    End Sub

#End Region

#Region "Group Control Events"

    Public Function validateGroupNameAndLabel(name As String, label As String, isNewGroup As Boolean) As Boolean

        If Not validateNameFormat(name) Then
            Return False
        End If

        ' We must ensure that the group name is unique:

        Dim groupList As New List(Of Group)

        groupList.AddRange(groups)

        Dim nameSearch = groupList.FindAll(Function(grp As Group)
                                               Return grp.name = name
                                           End Function)

        If nameSearch.Count > 1 Or (nameSearch.Count = 1 And isNewGroup) Then
            MsgBox("Name must be unique")
            Return False
        End If


        ' GROUP LABEL

        ' Cannot be empty string
        If label.Length <1 Then
            MsgBox("Label cannot be empty")
            Return False
        End If

        ' Passed Validation checks
        Return True

    End Function

    Private Sub btn_groupSave_Click(sender As Object, e As EventArgs) Handles btn_groupSave.Click

        ' Perform validation on the name and label
        If Not validateGroupNameAndLabel(txt_groupName.Text, txt_groupLabel.Text, areThereAnyUnsavedNewGroups()) Then
            Return
        End If

        Dim currentTab As TabPage = tb_ctrl.SelectedTab ' Gets the current TabPage

        If areThereAnyUnsavedNewGroups() Then '  This is for new group

            ' For a new group we need to have at least one setting, but we need to have that setting made before we create the group object otherwise we get an exception

            Dim placeholderSetting As Binary = New Binary("placeholderSetting", "This is a placeholder for a new group", 1, True, True)

            Dim newGroup As Group = New Group(txt_groupName.Text, txt_groupLabel.Text, num_groupAccessLevel.Value, {placeholderSetting})

            groups.Add(newGroup)

            currentTab.Text = newGroup.name ' Update the TabPage text to new name

        Else ' Editing existing group

            Dim currentGroup As Group = getCurrentSelectedGroup()

            currentGroup.name = txt_groupName.Text
            currentGroup.label = txt_groupLabel.Text
            currentGroup.accessLevel = num_groupAccessLevel.Value

            currentTab.Text = currentGroup.name ' Update the TabPage text to new name

        End If

        refreshCurrentList()

        ' Finally re-enable the tb_ctrl, btn_newSetting and btn_newGroup
        toggleMainControlState(True)

        MsgBox("Group Saved")

    End Sub

    Private Sub btn_groupDelete_Click(sender As Object, e As EventArgs) Handles btn_groupDelete.Click

        ' Need to have a check here for how many groups there are, must have a minimum of one group

        If tb_ctrl.TabPages.Count < 2 Then
            MsgBox("You must have a minimum of one group")
            Return
        End If

        ' Prompt to the user to confirm delete
        If MsgBox("Are you sure you wish to delete this Group, and all of its children?", MsgBoxStyle.YesNo, "Confirm Group Delete") = MsgBoxResult.No Then
            Return
        End If

        If Not areThereAnyUnsavedNewGroups() Then ' If existing group then we also need to remove the group from the group collection

            Dim currentGroup As Group = getCurrentSelectedGroup()

            groups.Remove(currentGroup)

        End If

        Dim currentTab = tb_ctrl.SelectedTab

        tb_ctrl.TabPages.Remove(currentTab) ' Updates the tab control to not show the deleted group

        toggleMainControlState(True)

    End Sub

    Private Sub txt_groupName_Leave(sender As Object, e As EventArgs) Handles txt_groupName.Leave
        txt_groupName.Text = txt_groupName.Text.Trim() ' Remove all whitespace at both ends of the string
    End Sub

    Private Sub txt_groupLabel_Leave(sender As Object, e As EventArgs) Handles txt_groupLabel.Leave
        txt_groupLabel.Text = txt_groupLabel.Text.Trim() ' Remove all whitespace at both ends of the string
    End Sub


#End Region

#Region "Setting Control Events"

    Public Function validateSettingNameAndLabel(name As String, label As String, isNewSetting As Boolean) As Boolean

        If Not validateNameFormat(name) Then
            Return False
        End If

        ' We must ensure that the setting name is unique:
        Dim allSettings As New List(Of Setting)

        For Each group As Group In groups

            allSettings.AddRange(group.getSettings())

        Next

        Dim nameSearch = allSettings.FindAll(Function(val As Setting)
                                                 Return val.name = name
                                             End Function)

        If nameSearch.Count > 1 Or (nameSearch.Count = 1 And isNewSetting) Then
            MsgBox("Name must be unique")
            Return False
        End If

        ' Setting LABEL

        ' Cannot be empty string
        If label.Length < 1 Then
            MsgBox("Label cannot be empty")
            Return False
        End If


        ' Passed Validation checks
        Return True

    End Function

    Public Function validateNameFormat(name As String) As Boolean

        If name.Length < 1 Then
            MsgBox("Name cannot be empty")
            Return False
        End If

        ' Setting name cannot have whitespace inside
        If name.Split(" ").Length > 1 Then
            MsgBox("Name cannot include whitespace")
            Return False
        End If

        ' Restrict to only alphabetical characters
        If Not Regex.IsMatch(name, "^[a-zA-Z][a-zA-Z0-9]*$") Then
            MsgBox("Name cannot include non-alphanumeric characters or start with a number")
            Return False
        End If

        Return True

    End Function

    ' Takes setting object and loads relevant properties into the gb_settingProperties box depending on the setting type
    Private Sub loadSettingIntoGroupBox(setting As Setting)

        hideAndClearValueControls()

        Dim settingType = setting.GetType().Name

        ' Set property controls to existing values
        txt_settingName.Text = setting.name
        txt_settingLabel.Text = setting.label
        num_settingAccessLevel.Value = setting.accessLevel
        cb_settingViewIfNoAccess.Checked = setting.viewIfNoAccess
        cb_settingType.SelectedItem = settingType

        cb_settingType.Enabled = False ' Disable the control as shouldn't be allowed to change type

        ' Create value control
        Select Case settingType

            Case "Text"

                Dim text As Text = setting

                lbl_maxLength.Visible = True
                num_maxLength.Visible = True
                num_maxLength.Value = text.maxLength

                txt_defaultValue.Visible = True
                txt_defaultValue.Text = text.defaultValue ' We read and write the defaultValue which directly accesses _value as we might not be read/writing from an XML

            Case "Numeric"

                Dim numeric As Numeric = setting

                num_defaultValue.Visible = True

                lbl_decimalPlaces.Visible = True
                num_decimalPlaces.Visible = True

                lbl_maxValue.Visible = True
                num_maxValue.Visible = True

                lbl_minValue.Visible = True
                num_minValue.Visible = True

                ' Set control values to match setting
                num_defaultValue.DecimalPlaces = numeric.decimalPlaces ' TODO - Test this works okay switching between settings new/old with different DP
                num_defaultValue.Minimum = numeric.minValue
                num_defaultValue.Maximum = numeric.maxValue
                num_defaultValue.Value = numeric.defaultValue


                num_decimalPlaces.Value = numeric.decimalPlaces

                num_maxValue.Value = numeric.maxValue
                num_minValue.Value = numeric.minValue

            Case "Binary"

                Dim binary As Binary = setting

                chk_defaultValue.Visible = True
                chk_defaultValue.Checked = binary.defaultValue

            Case "Picklist"

                Dim picklist As Picklist = setting

                cb_defaultValue.Visible = True
                btn_editOptions.Visible = True

                cb_defaultValue.Items.Clear()

                cb_defaultValue.Items.AddRange(picklist.getOptionLabels())

                If picklist.options.Count > 0 Then

                    Dim selectedOption = picklist.getOptionByValue(picklist.defaultValue)

                    cb_defaultValue.SelectedItem = selectedOption.label

                End If

            Case "ComPortList"

                Dim comPortList As ComPortList = setting

                num_defaultValue.Visible = True

                num_defaultValue.Increment = 1
                num_defaultValue.DecimalPlaces = 0
                num_defaultValue.Minimum = 0
                num_defaultValue.Maximum = 255
                num_defaultValue.Value = comPortList.defaultValue

            Case "PrinterList"

                'Don't need to have any controls for setting default

            Case "PathLocation"

                Dim pathLocation As PathLocation = setting

                ' Don't need to have any controls for setting default path as the path won't be relevant on the client machine
                chk_isFilePath.Checked = pathLocation.isFilePath

        End Select

    End Sub

    Private Sub loadSubgroupintoGroupBox(subgroup As Subgroup)

        txt_subgroupName.Text = subgroup.name
        txt_subgroupLabel.Text = subgroup.label
        num_subgroupAccessLevel.Value = subgroup.accessLevel

    End Sub

    Private Sub childSelection(ByVal sender As System.Object, ByVal e As System.EventArgs) ' This handles the afterSelect event on the treeview so could be either subgroup or setting

        Dim treeView As TreeView = sender

        ' Reset all group box visability so we know where we're at
        gb_settingProperties.Visible = False
        gb_groupProperties.Visible = False
        gb_subgroupProperties.Visible = False

        Dim selectedGroup As Group = getCurrentSelectedGroup()

        Dim selectedChild As Child = selectedGroup.getChildByName(treeView.SelectedNode.Text)

        '' TODO - test if this is still required and only if the selectedNode is null of something
        If selectedChild Is Nothing Then ' This prevents event when not anything selected, or when child does not exist yet?
            Return
        End If

        If selectedChild.GetType().Name = "Subgroup" Then ' If subgroup then we show subgroup settings which are amended group properties

            Dim subgroup As Subgroup = selectedChild ' Convert to subgroup

            loadSubgroupintoGroupBox(subgroup)

            gb_subgroupProperties.Visible = True

        Else ' If setting then we load setting properties just as before

            Dim selectedSetting As Setting = selectedChild ' Convert to setting

            loadSettingIntoGroupBox(selectedSetting)

            gb_settingProperties.Visible = True ' Show setting properties

        End If

    End Sub

    Private Sub btn_settingSave_Click(sender As Object, e As EventArgs) Handles btn_settingSave.Click ' Save what is currently in the setting properties to the setting selected in the list box of the current group

        Dim unsavedSettingsBool As Boolean = areThereAnyUnsavedNewSettings() ' value used mulitple times so save once to improve performance

        ' Perform validation on the name and label
        If Not validateSettingNameAndLabel(txt_settingName.Text, txt_settingLabel.Text, unsavedSettingsBool) Then
            Return
        End If

        ' Perform some validation on the properties and the default value to ensure all is well
        Select Case cb_settingType.SelectedItem

            Case "Text"

                ' Ensure default value length is within maxLength given
                If txt_defaultValue.Text.Length > num_maxLength.Value Then
                    MsgBox("Default Text value cannot exceed Max Length provided")
                    Return
                End If

            Case "Numeric"

                ' Ensure value is between min and max
                If num_defaultValue.Value < num_minValue.Value Or num_defaultValue.Value > num_maxValue.Value Then
                    MsgBox("Default Numeric value must be within the minimum and maximum bounds provided")
                    Return
                End If

        End Select



        If unsavedSettingsBool Then ' Create new setting object

            Dim newSetting

            Select Case cb_settingType.SelectedItem

                Case "Text"

                    newSetting = New Text(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, txt_defaultValue.Text, num_maxLength.Value)

                Case "Numeric"

                    newSetting = New Numeric(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, num_defaultValue.Value, num_decimalPlaces.Value, num_maxValue.Value, num_minValue.Value)

                Case "Binary"

                    newSetting = New Binary(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, chk_defaultValue.Checked)

                Case "Picklist"

                    newSetting = New Picklist(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, cb_defaultValue.SelectedIndex, {}) ' We create blank picklist options as we have to save the setting before adding options

                Case "ComPortList"

                    newSetting = New ComPortList(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, num_defaultValue.Value)

                Case "PrinterList"

                    newSetting = New PrinterList(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, "")

                Case "PathLocation"

                    newSetting = New PathLocation(txt_settingName.Text, txt_settingLabel.Text, num_settingAccessLevel.Value, cb_settingViewIfNoAccess.Checked, "", chk_isFilePath.Checked)

                Case Else

                    Throw New Exception("btn_settingSave_Click: Setting type invalid")

            End Select

            Dim currentGroup As Group = getCurrentSelectedGroup()

            Dim treeView As TreeView = getCurrentSettingsTreeView()

            Dim selectedNode As TreeNode = treeView.SelectedNode

            If selectedNode.Parent IsNot Nothing Then ' Setting member of subgroup

                Dim subgroupNode As TreeNode = selectedNode.Parent

                Dim subgroup As Subgroup = currentGroup.getChildByName(subgroupNode.Text)

                subgroup.children.Add(newSetting) ' Add new setting to subgroups children

            Else

                currentGroup.children.Add(newSetting) ' Add new setting to group children

            End If

        Else ' Editing existing setting

            Dim selectedSetting = getCurrentSelectedSetting()

            Try
                Dim settingType = selectedSetting.GetType().Name

                ' Change properties to corresponding control values
                selectedSetting.name = txt_settingName.Text
                selectedSetting.label = txt_settingLabel.Text
                selectedSetting.accessLevel = num_settingAccessLevel.Value
                selectedSetting.viewIfNoAccess = cb_settingViewIfNoAccess.Checked

                ' Create value control
                Select Case settingType

                    Case "Text"

                        selectedSetting.defaultValue = txt_defaultValue.Text
                        selectedSetting.maxLength = num_maxLength.Value

                    Case "Numeric"

                        selectedSetting.defaultValue = num_defaultValue.Value

                        ' TODO - I've just realised these properties are READONLY with regards to XML editing
                        selectedSetting.decimalPlaces = num_decimalPlaces.Value
                        selectedSetting.maxValue = num_maxValue.Value
                        selectedSetting.minValue = num_minValue.Value

                    Case "Binary"
                        selectedSetting.defaultValue = chk_defaultValue.Checked

                    Case "Picklist"

                        Dim selectedOption As PicklistOption = selectedSetting.options(cb_defaultValue.SelectedIndex) ' Indices should match up

                        selectedSetting.defaultValue = selectedOption.value

                    Case "ComPortList"
                        selectedSetting.defaultValue = num_defaultValue.Value

                    Case "PrinterList"
                        ' Do nothing

                    Case "PathLocation"
                        selectedSetting.isFilePath = chk_isFilePath.Checked

                End Select

            Catch ex As Exception
                clsAppFunctions.ErrMsg(sender, "btn_settingSave.Click", ex)
            End Try

        End If

        ' Finally re-enable the tb_ctrl, btn_newSetting and btn_newGroup
        toggleMainControlState(True)

        refreshCurrentList()

        gb_groupProperties.Visible = True
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False

        MsgBox("Setting Saved")

    End Sub

    Private Sub btn_settingDelete_Click(sender As Object, e As EventArgs) Handles btn_settingDelete.Click

        ' Prompt to the user to confirm delete
        If MsgBox("Are you sure you wish to delete this Setting?", MsgBoxStyle.YesNo, "Confirm Setting Delete") = MsgBoxResult.No Then
            Return
        End If

        toggleMainControlState(True)

        Dim currentSetting = getCurrentSelectedSetting()

        If currentSetting IsNot Nothing Then ' Bypass functions if deleting not already saved setting

            Dim currentGroup As Group = getCurrentSelectedGroup()

            If currentGroup.children.Contains(currentSetting) Then

                currentGroup.children.Remove(currentSetting)

            Else ' TODO - consider case of setting being within subgroup

                Dim treeView As TreeView = getCurrentSettingsTreeView()

                Dim selectedNode As TreeNode = treeView.SelectedNode

                If selectedNode.Parent IsNot Nothing Then

                    Dim subgroupNode As TreeNode = selectedNode.Parent

                    Dim subgroup As Subgroup = currentGroup.getChildByName(subgroupNode.Text)

                    If subgroup.children.Count < 2 Then
                        MsgBox("Subgroups must have at least one child")
                        Return
                    End If

                    subgroup.children.Remove(currentSetting)

                End If

            End If

        End If

        refreshCurrentList() ' Update the list to show settings has been removed

        ' Switch to group properties view
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False
        gb_groupProperties.Visible = True


    End Sub

    Private Sub cb_settingType_SelectedValueChanged() Handles cb_settingType.SelectedValueChanged

        ' Clear form
        hideAndClearValueControls()

        Select Case cb_settingType.SelectedItem

            Case "Text"

                txt_defaultValue.Visible = True
                lbl_maxLength.Visible = True
                num_maxLength.Visible = True

            Case "Numeric"

                num_defaultValue.Visible = True
                lbl_decimalPlaces.Visible = True
                num_decimalPlaces.Visible = True
                lbl_maxValue.Visible = True
                num_maxValue.Visible = True
                lbl_minValue.Visible = True
                num_minValue.Visible = True

                num_maxValue.Value = 1000
                num_defaultValue.Maximum = num_maxValue.Value
                num_defaultValue.Minimum = num_minValue.Value

            Case "Binary"
                chk_defaultValue.Visible = True

            Case "Picklist"

                cb_defaultValue.Visible = True
                btn_editOptions.Visible = True

            Case "ComPortList"

                num_defaultValue.Visible = True
                num_defaultValue.Increment = 1
                num_defaultValue.DecimalPlaces = 0
                num_defaultValue.Minimum = 0
                num_defaultValue.Maximum = 255

            Case "PrinterList"
                ' do nothing

            Case "PathLocation"
                chk_isFilePath.Visible = True

        End Select

    End Sub

    Private Sub btn_editOptions_Click(sender As Object, e As EventArgs) Handles btn_editOptions.Click

        Dim currentSetting = getCurrentSelectedSetting() ' Get current setting

        If currentSetting.GetType().Name <> "Picklist" Then ' This is for when setting object doesn't exist yet
            MsgBox("You must save the setting before you can edit the options")
            Return
        End If

        Dim picklist As Picklist = currentSetting

        ' Create instance of the editOptions form passing the options through the constructor
        Dim frmEditOption = New frm_editOptions(picklist.name, picklist.options)

        ' If the user selects Save Options on frm_editOptions then the options are updated in the Picklist object
        If frmEditOption.ShowDialog() = DialogResult.OK Then

            picklist.options = frmEditOption.Options ' Save options in the setting object

            cb_defaultValue.Items.Clear() ' Remove the previous options

            cb_defaultValue.Items.AddRange(picklist.getOptionLabels()) ' Update the default value options

            cb_defaultValue.SelectedIndex = 0 ' Set to the first option as otherwise will end up being null

        End If

    End Sub

    Private Sub txt_settingName_Leave(sender As Object, e As EventArgs) Handles txt_settingName.Leave
        txt_settingName.Text = txt_settingName.Text.Trim()
    End Sub

    Private Sub txt_settingLabel_Leave(sender As Object, e As EventArgs) Handles txt_settingLabel.Leave
        txt_settingLabel.Text = txt_settingLabel.Text.Trim()
    End Sub

    Private Sub tb_ctrl_GotFocus(sender As Object, e As EventArgs) Handles tb_ctrl.GotFocus ' This is just so that we can return to group properties when there is only one tab
        gb_groupProperties.Visible = True
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False
    End Sub

    Private Sub num_maxValue_ValueChanged(sender As Object, e As EventArgs) Handles num_maxValue.ValueChanged
        num_defaultValue.Maximum = num_maxValue.Value
    End Sub

    Private Sub num_minValue_ValueChanged(sender As Object, e As EventArgs) Handles num_minValue.ValueChanged
        num_defaultValue.Minimum = num_minValue.Value
    End Sub

    Private Sub btn_saveSubgroup_Click(sender As Object, e As EventArgs) Handles btn_subgroupSave.Click

        ' TODO - similiar to btn_groupSave
        Dim unsavedSubgroupsBool As Boolean = areThereAnyUnsavedNewSubgroups()
        ' Perform validation on the name and label
        If Not validateGroupNameAndLabel(txt_subgroupName.Text, txt_subgroupLabel.Text, unsavedSubgroupsBool) Then
            Return
        End If

        Dim currentGroup As Group = getCurrentSelectedGroup()

        If unsavedSubgroupsBool Then '  This is for new subgroup

            ' For a new subgroup we need to have at least one setting, but we need to have that setting made before we create the group object otherwise we get an exception

            Dim placeholderSetting As Binary = New Binary("placeholderSetting", "This is a placeholder for a new group", 1, True, True)

            Dim newSubgroup As Subgroup = New Subgroup(txt_subgroupName.Text, txt_subgroupLabel.Text, num_subgroupAccessLevel.Value, {placeholderSetting})

            currentGroup.children.Add(newSubgroup) ' Add subgroup to the current group object's children

        Else ' Editing existing group
            Try

                Dim selectedChild As Child = getCurrentSelectedChild()

                If selectedChild.GetType().Name <> "Subgroup" Then
                    MsgBox("ERROR: Subgroup node not selected")
                End If

                Dim existingSubgroup As Subgroup = selectedChild

                existingSubgroup.name = txt_subgroupName.Text
                existingSubgroup.label = txt_subgroupLabel.Text
                existingSubgroup.accessLevel = num_subgroupAccessLevel.Value

            Catch ex As Exception
                clsAppFunctions.ErrMsg(sender, "btn_saveSubgroupClick", ex)
            End Try

        End If

        refreshCurrentList()

        ' Finally re-enable the tb_ctrl, btn_newSetting and btn_newGroup
        toggleMainControlState(True)

        MsgBox("Subgroup Saved")
    End Sub

    Private Sub btn_deleteSubgroup_Click(sender As Object, e As EventArgs) Handles btn_subgroupDelete.Click

        ' Prompt to the user to confirm delete
        If MsgBox("Are you sure you wish to delete this Subgroup?", MsgBoxStyle.YesNo, "Confirm Subgroup Delete") = MsgBoxResult.No Then
            Return
        End If

        toggleMainControlState(True)

        Dim currentSubgroup As Subgroup = getCurrentSelectedChild()

        If currentSubgroup IsNot Nothing Then ' Bypass functions if deleting not already saved setting

            Dim currentGroup As Group = getCurrentSelectedGroup()

            currentGroup.children.Remove(currentSubgroup)

        End If

        refreshCurrentList() ' Update the list to show settings has been removed

        ' Switch to group properties view
        gb_settingProperties.Visible = False
        gb_subgroupProperties.Visible = False
        gb_groupProperties.Visible = True

    End Sub

    Private Sub txt_subgroupName_Leave(sender As Object, e As EventArgs) Handles txt_subgroupName.Leave
        txt_subgroupName.Text = txt_subgroupName.Text.Trim()
    End Sub

    Private Sub txt_subgroupLabel_Leave(sender As Object, e As EventArgs) Handles txt_subgroupLabel.Leave
        txt_subgroupLabel.Text = txt_subgroupLabel.Text.Trim()
    End Sub

#End Region

End Class
