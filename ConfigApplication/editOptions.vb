﻿

Imports clsSettingsManager

Public Class frm_editOptions

    Private _options As New List(Of PicklistOption)

    Public Sub New(settingName As String, options As List(Of PicklistOption))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Me.Text = settingName & " Options" ' Set title of form


        If options.Count < 1 Then ' Ensure there is at least one option (as new Picklist will not have any)

            Dim newOption As PicklistOption = New PicklistOption("", "", 0) ' Create Blank option

            Me.Options.Add(newOption)

        Else

            ' Rather than add the objects as this will only pass the references of the PicklistOption objects
            ' Lets create duplicates of the objects
            For Each opt In options
                Me.Options.Add(New PicklistOption(opt.name, opt.label, opt.value))
            Next

        End If

        dgv_optionDetails.DataSource = Nothing

        dgv_optionDetails.DataSource = Me.Options ' Update DGV with the options list

        btn_optionDelete.Enabled = False ' Set Delete disabled as default as this will allow us to properly select the row

    End Sub

    Public Property Options As List(Of PicklistOption)
        Get
            Return _options
        End Get
        Set(value As List(Of PicklistOption))
            _options = value
        End Set
    End Property

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click

        ' TODO - add validation:

        ' TODO - All cells are non empty

        ' TODO - name rows are one word only containing characters and no symbols

        ' Picklist must have at least one options
        If Me.Options.Count < 1 Then
            MsgBox("You must have at least one option in the list")
            Return
        End If



        ' Update the frm_editOptions Option property to be accessible by mainConfig when we return
        Me.Options = dgv_optionDetails.DataSource

        For Each opt In Me.Options

            If Not mainConfig.validateNameFormat(opt.name) Then
                Return
            End If

            If String.IsNullOrWhiteSpace(opt.label) Then
                MsgBox("Option label cannot be empty")
                Return
            End If

            ' Checks if the names are unique within the 
            Dim nameSearch = Me.Options.FindAll(Function(o As PicklistOption)
                                                    Return o.name = opt.name
                                                End Function)

            If nameSearch.Count > 1 Then
                MsgBox("Option names must be unique within Picklist")
                Return
            End If

            Dim valSearch = Me.Options.FindAll(Function(o As PicklistOption)
                                                   Return o.value = opt.value
                                               End Function)

            If Not IsNumeric(opt.value) Then
                MsgBox("Option values must be numeric")
                Return
            End If

            If valSearch.Count > 1 Then
                MsgBox("Options must have unique values within Picklist")
                Return
            End If



        Next

        ' Notice to the User about the default value when they return to the main screen
        MsgBox("If you have made changes you will need to adjust the default value accordingly when you return to the main page")


        ' Allows use to check in mainConfig that the form was saved and therefore to update the Picklist.options property
        Me.DialogResult = System.Windows.Forms.DialogResult.OK

        Me.Close()

    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click

        If MsgBox("Are you sure you want to discard any changes?", MsgBoxStyle.YesNo, "Discard changes?") = MsgBoxResult.Yes Then
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Close()
        End If

    End Sub

    ' Add blank PicklistOption to the DGV
    Private Sub btn_optionAdd_Click(sender As Object, e As EventArgs) Handles btn_optionAdd.Click

        ' Automatically calculate the next value for the new option 
        Dim compareList = New List(Of PicklistOption)
        compareList.AddRange(Me.Options)
        Dim maxOptVal = compareList.OrderByDescending(Function(o) o.value).FirstOrDefault

        Dim newOption = New PicklistOption("", "", maxOptVal.value + 1)

        Me.Options.Add(newOption)

        dgv_optionDetails.DataSource = Nothing

        dgv_optionDetails.DataSource = Me.Options ' Update the DGV with the new options list

    End Sub

    'Delete currently selected row from the options
    Private Sub btn_optionDelete_Click(sender As Object, e As EventArgs) Handles btn_optionDelete.Click

        Dim selectedRowIndex = dgv_optionDetails.SelectedCells(0).RowIndex

        Me.Options.RemoveAt(selectedRowIndex)

        dgv_optionDetails.DataSource = Nothing

        dgv_optionDetails.DataSource = Me.Options

    End Sub

    Private Sub dgv_optionDetails_SelectionChanged(sender As Object, e As EventArgs) Handles dgv_optionDetails.SelectionChanged
        ' Disable the delete button depending on the selection (just an easy way to prevent errors for btn_optionDelete_Click)
        btn_optionDelete.Enabled = dgv_optionDetails.SelectedCells.Count > 0
    End Sub

End Class
