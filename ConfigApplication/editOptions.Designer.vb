﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_editOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.btn_optionDelete = New System.Windows.Forms.Button()
        Me.btn_optionAdd = New System.Windows.Forms.Button()
        Me.dgv_optionDetails = New System.Windows.Forms.DataGridView()
        Me.btn_cancel = New System.Windows.Forms.Button()
        CType(Me.dgv_optionDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_save
        '
        Me.btn_save.Location = New System.Drawing.Point(200, 211)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(56, 37)
        Me.btn_save.TabIndex = 0
        Me.btn_save.Text = "Save Options"
        '
        'btn_optionDelete
        '
        Me.btn_optionDelete.Location = New System.Drawing.Point(104, 211)
        Me.btn_optionDelete.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_optionDelete.Name = "btn_optionDelete"
        Me.btn_optionDelete.Size = New System.Drawing.Size(56, 37)
        Me.btn_optionDelete.TabIndex = 23
        Me.btn_optionDelete.Text = "Delete Option"
        Me.btn_optionDelete.UseVisualStyleBackColor = True
        '
        'btn_optionAdd
        '
        Me.btn_optionAdd.Location = New System.Drawing.Point(9, 211)
        Me.btn_optionAdd.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_optionAdd.Name = "btn_optionAdd"
        Me.btn_optionAdd.Size = New System.Drawing.Size(56, 37)
        Me.btn_optionAdd.TabIndex = 22
        Me.btn_optionAdd.Text = "Add Option"
        Me.btn_optionAdd.UseVisualStyleBackColor = True
        '
        'dgv_optionDetails
        '
        Me.dgv_optionDetails.AllowUserToResizeColumns = False
        Me.dgv_optionDetails.AllowUserToResizeRows = False
        Me.dgv_optionDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_optionDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_optionDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_optionDetails.Location = New System.Drawing.Point(9, 10)
        Me.dgv_optionDetails.Margin = New System.Windows.Forms.Padding(2)
        Me.dgv_optionDetails.MultiSelect = False
        Me.dgv_optionDetails.Name = "dgv_optionDetails"
        Me.dgv_optionDetails.RowHeadersWidth = 51
        Me.dgv_optionDetails.RowTemplate.Height = 24
        Me.dgv_optionDetails.Size = New System.Drawing.Size(342, 196)
        Me.dgv_optionDetails.TabIndex = 21
        '
        'btn_cancel
        '
        Me.btn_cancel.Location = New System.Drawing.Point(295, 210)
        Me.btn_cancel.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(56, 37)
        Me.btn_cancel.TabIndex = 24
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'frm_editOptions
        '
        Me.AcceptButton = Me.btn_save
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 282)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_cancel)
        Me.Controls.Add(Me.btn_save)
        Me.Controls.Add(Me.btn_optionDelete)
        Me.Controls.Add(Me.btn_optionAdd)
        Me.Controls.Add(Me.dgv_optionDetails)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_editOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Picklist Options"
        CType(Me.dgv_optionDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_save As System.Windows.Forms.Button
    Friend WithEvents btn_optionDelete As Button
    Friend WithEvents btn_optionAdd As Button
    Friend WithEvents dgv_optionDetails As DataGridView
    Friend WithEvents NameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LabelDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents btn_cancel As Button
End Class
